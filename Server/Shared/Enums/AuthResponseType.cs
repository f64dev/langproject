﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Server.Shared.Enums
{
    public enum AuthResponseType
    {
        Ok=0,
        LoginInvalid=1,
        LoginExists=2,
        EmailInvalid=4,
        EmailExists=8,
        PassInvalid=16
    }
}
