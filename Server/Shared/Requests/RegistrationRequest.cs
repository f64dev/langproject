﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Server.Shared.Requests
{
    public class RegistrationRequest : LoginRequest
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }

        public DateTime Birthdate { get; set; }
        public Gender Gender { get; set; }
    }
}
