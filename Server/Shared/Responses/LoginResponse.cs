﻿using System;
using System.Collections.Generic;
using System.Text;
using Server.Shared.Enums;

namespace Server.Shared.Responses
{
    public class LoginResponse
    {
        public string Token { get; set; }
        public AuthResponseType Type { get; set; }
    }
}
