﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;

namespace Server.API.Auth
{
    public class AuthOptions
    {
        #region Init

        public const string Issuer = "EnglishAppServer";
        public const string Audience = "EnglishApp";
        public const int Lifetime = 6;

        #endregion
        #region Key

        const string Key = "Zi3RQH6Y64XUk2fFCiHVsxYWIEy6Petc";
        public static SymmetricSecurityKey GetSymmetricSecurityKey() => new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Key));

        #endregion
        #region Option

        public static void SetOptions(JwtBearerOptions options)
        {
            options.RequireHttpsMetadata = false;
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = Issuer,

                ValidateAudience = true,
                ValidAudience = Audience,

                ValidateLifetime = true,


                ValidateIssuerSigningKey = true,
                IssuerSigningKey = GetSymmetricSecurityKey()
            };
        }

        #endregion
    }
}
