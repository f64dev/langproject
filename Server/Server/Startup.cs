﻿using System;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Server.API.Auth;
using Server.Services;

namespace Server.API
{
    public class Startup
    {
        #region Init

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        #endregion
        #region Services

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<Contexts.AppDBContext>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(AuthOptions.SetOptions);

            services.AddSingleton(Configuration);

            services.AddSingleton<DropboxService>();

            services.AddMvc();
        }

        #endregion
        #region Configure

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

        //    app.UseDefaultFiles();
         //   app.UseStaticFiles();

            app.UseAuthentication();
            app.UseMvc();
        } 

        #endregion
    }
}
