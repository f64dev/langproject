﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dropbox.Api;
using Microsoft.Extensions.Configuration;

namespace Server.Services
{
    public class DropboxService
    {
        #region Init

        private DropboxClient client;

        public DropboxService(IConfiguration configuration)
        {
            client = new DropboxClient(configuration["Dropbox:Token"]);
        }

        #endregion
        #region Download

        public async Task<string> DownloadString(string path)
        {
            string value = null;

            try
            {
                using (var response = await client.Files.DownloadAsync($"/{path}"))
                    value = await response.GetContentAsStringAsync();
            }
            catch { }
           
            return    value;
        }

        public async Task<Stream> DownloadStream(string path)
        {
            Stream value = null;

            try
            {
                using (var response = await client.Files.DownloadAsync($"/{path}"))
                    value = await response.GetContentAsStreamAsync();
            }
            catch { }

            return value;
        }

        #endregion
    }
}
