﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Server.API.Models.DB;

namespace Server.API.Contexts
{
    public class AppDBContext:DbContext
    {
        public DbSet<User> Users { get; set; }


        #region Configure

 
        public AppDBContext()
        {
           
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
         //   optionsBuilder.UseMySQL(configuration.GetConnectionString("MySQLConnection"));

            optionsBuilder.UseMySQL("Server=MYSQL6002.site4now.net;Database=db_a3c3e1_users;Uid=a3c3e1_users;Pwd=W3t3D5AV1488;SslMode=none;");
        }

        #endregion
    }
}
