﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Server.API.Auth;
using Server.API.Contexts;
using Server.API.Models.DB;
using Server.Shared.Enums;
using Server.Shared.Requests;
using Server.Shared.Responses;

namespace Server.API.Controllers
{
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        #region Init 

        private AppDBContext db;

        public AuthController(AppDBContext dbContext)
        {
            db = dbContext;
        }

        #endregion
        #region Login

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody]LoginRequest login)
        {
            var (user, type) = await GetUser(login);

            if (type == AuthResponseType.Ok)
                return Ok(new LoginResponse() { Token = BuildToken(user), Type = type });

            return Ok(new LoginResponse() { Type = type });
        }

        private async Task<(User, AuthResponseType)> GetUser(LoginRequest data)
        {
            User user = null;

            var type = AuthResponseType.Ok;

            if (!string.IsNullOrEmpty(data.Email))
            {
                if (CheckEmail(data.Email))
                {
                    user = await db.Users.Include(p => p.LoginData).FirstOrDefaultAsync(p => p.LoginData.Email == data.Email);

                    if (user == null)
                        type |= AuthResponseType.EmailExists;
                }
                else
                    type |= AuthResponseType.EmailInvalid;
            }
            else if (!string.IsNullOrEmpty(data.Login))
            {
                if (CheckLogin(data.Login))
                {
                    user = await db.Users.Include(p => p.LoginData).FirstOrDefaultAsync(p => p.LoginData.Email == data.Email);

                    if (user == null)
                        type |= AuthResponseType.LoginExists;
                }
                else
                    type |= AuthResponseType.LoginInvalid;
            }


            if (user == null)
                return (user, type);


            if (user.LoginData.Password != data.Password)
                type |= AuthResponseType.PassInvalid;

            return (user, type);
        }

        #endregion
        #region Registration


        [Authorize]
        [HttpGet("test")]
        public async Task<IActionResult> Test()
        {
            return Ok(new RegistrationResponse() {Type=AuthResponseType.Ok|AuthResponseType.PassInvalid });
        }


        private const string apiUrl = "http://localhost:49229/api/";

        private const string testUrl = apiUrl + "auth/test";

        [AllowAnonymous]
        [HttpGet("test2")]
        public async Task Test2()
        {
            var client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;


            var uri = new Uri(testUrl);

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1MzA4MDE5NDgsImlzcyI6IkVuZ2xpc2hBcHBTZXJ2ZXIiLCJhdWQiOiJFbmdsaXNoQXBwIn0.vodqK2Wp31acAn9-2feJ5GDOZBaoaXJwZplUdALCnAY");

            var response = await client.GetAsync(uri);

            if (response.IsSuccessStatusCode)
                Debug.WriteLine(await response.Content.ReadAsStringAsync());
        }





        [AllowAnonymous]
        [HttpPost("registration")]
        public async Task<IActionResult> Registration([FromBody]RegistrationRequest data)
        {
            var type = await GetRegistrationType(data);

            if (type != AuthResponseType.Ok)
                return Ok(new RegistrationResponse() { Type = type });

            var user = new User()
            {
                Info = new UserInfo()
                {
                    FirstName = data.FirstName,
                    SecondName = data.SecondName,
                    Birthdate = data.Birthdate,
                    Gender = data.Gender

                },
                LoginData = new UserData()
                {
                    Email = data.Email,
                    Login = data.Login,
                    Password = data.Password,

                }
            };


            db.Users.Add(user);
            await db.SaveChangesAsync();

            return Ok(new RegistrationResponse() { Type = type , Token= BuildToken(user) });
        }


        private async Task<AuthResponseType> GetRegistrationType(RegistrationRequest data)
        {
            var type = AuthResponseType.Ok;

            if (CheckEmail(data.Email))
            {
                var user = await db.Users.Include(p => p.LoginData).FirstOrDefaultAsync(p => p.LoginData.Email == data.Email);

                if (user != null)
                    type |= AuthResponseType.EmailExists;
            }
            else
                type |= AuthResponseType.EmailInvalid;


            if (CheckLogin(data.Login))
            {
                var user = await db.Users.Include(p => p.LoginData).FirstOrDefaultAsync(p => p.LoginData.Login == data.Login);

                if (user != null)
                    type |= AuthResponseType.LoginExists;
            }
            else
                type |= AuthResponseType.LoginInvalid;


            if (!CheckPassword(data.Password))
                type |= AuthResponseType.PassInvalid;

            return type;
        }



        #endregion
        #region Token

        private string BuildToken(User user)
        {
            var key = AuthOptions.GetSymmetricSecurityKey();
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: AuthOptions.Issuer,
                audience: AuthOptions.Audience,
            //    claims: new List<Claim> { new Claim("id", user.Id.ToString()) },
                expires: DateTime.Now.AddDays(AuthOptions.Lifetime),
                signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        #endregion
        #region Check

        private bool CheckLogin(string data)
        {
            return true;
        }

        private bool CheckEmail(string data)
        {

            return true;
        }
        private bool CheckPassword(string data)
        {

            return true;
        }

        #endregion
    }
}
