﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.API.Models.DB
{
    public class UserData
    {
        public int Id { get; set; }

        public string Login { get; set; }
        public string Password { get; set; }

        public string Email { get; set; }
    }
}
