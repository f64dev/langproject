﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Server.Shared;

namespace Server.API.Models.DB
{
    public class UserInfo
    {
        public int Id { get; set; }

        public string FirstName { get; set; }
        public string SecondName { get; set; }

        public DateTime Birthdate { get; set; }
        public Gender Gender { get; set; }
    }
}
