﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.API.Models.DB
{
    public class User
    {
        public int Id { get; set; }
        public UserData LoginData { get; set; }
        public UserInfo Info { get; set; }
    }
}
