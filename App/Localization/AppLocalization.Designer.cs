﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Localization {
    using System;
    
    
    /// <summary>
    ///   Класс ресурса со строгой типизацией для поиска локализованных строк и т.д.
    /// </summary>
    // Этот класс создан автоматически классом StronglyTypedResourceBuilder
    // с помощью такого средства, как ResGen или Visual Studio.
    // Чтобы добавить или удалить член, измените файл .ResX и снова запустите ResGen
    // с параметром /str или перестройте свой проект VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class AppLocalization {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal AppLocalization() {
        }
        
        /// <summary>
        ///   Возвращает кэшированный экземпляр ResourceManager, использованный этим классом.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("App.Localization.AppLocalization", typeof(AppLocalization).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Перезаписывает свойство CurrentUICulture текущего потока для всех
        ///   обращений к ресурсу с помощью этого класса ресурса со строгой типизацией.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Authorization.
        /// </summary>
        public static string authorization_title {
            get {
                return ResourceManager.GetString("authorization_title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Registration 1\3.
        /// </summary>
        public static string BarTitle_Registration1 {
            get {
                return ResourceManager.GetString("BarTitle_Registration1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Registration 2\3.
        /// </summary>
        public static string BarTitle_Registration2 {
            get {
                return ResourceManager.GetString("BarTitle_Registration2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Registration 3\3.
        /// </summary>
        public static string BarTitle_Registration3 {
            get {
                return ResourceManager.GetString("BarTitle_Registration3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Sign in.
        /// </summary>
        public static string BarTitle_SignIn {
            get {
                return ResourceManager.GetString("BarTitle_SignIn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Next.
        /// </summary>
        public static string Button_Next {
            get {
                return ResourceManager.GetString("Button_Next", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Registration.
        /// </summary>
        public static string Button_Registration {
            get {
                return ResourceManager.GetString("Button_Registration", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Sign in.
        /// </summary>
        public static string Button_SignIn {
            get {
                return ResourceManager.GetString("Button_SignIn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Date of birth.
        /// </summary>
        public static string DateOfBirthLabel {
            get {
                return ResourceManager.GetString("DateOfBirthLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на First Name.
        /// </summary>
        public static string FirstNamePlaceholder {
            get {
                return ResourceManager.GetString("FirstNamePlaceholder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Forgot your password?.
        /// </summary>
        public static string forgot_password {
            get {
                return ResourceManager.GetString("forgot_password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Gender.
        /// </summary>
        public static string GenderLabel {
            get {
                return ResourceManager.GetString("GenderLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Date of birth.
        /// </summary>
        public static string Label_DateOfBirth {
            get {
                return ResourceManager.GetString("Label_DateOfBirth", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Gender.
        /// </summary>
        public static string Label_Gender {
            get {
                return ResourceManager.GetString("Label_Gender", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Female.
        /// </summary>
        public static string Label_GenderFemale {
            get {
                return ResourceManager.GetString("Label_GenderFemale", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Male.
        /// </summary>
        public static string Label_GenderMale {
            get {
                return ResourceManager.GetString("Label_GenderMale", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Other.
        /// </summary>
        public static string Label_GenderOther {
            get {
                return ResourceManager.GetString("Label_GenderOther", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Last Name.
        /// </summary>
        public static string LastNamePlaceholder {
            get {
                return ResourceManager.GetString("LastNamePlaceholder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на E-mail or Login.
        /// </summary>
        public static string login_placeholder {
            get {
                return ResourceManager.GetString("login_placeholder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Password.
        /// </summary>
        public static string password_placeholder {
            get {
                return ResourceManager.GetString("password_placeholder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Confirm Password.
        /// </summary>
        public static string Placeholder_ConfirmPassword {
            get {
                return ResourceManager.GetString("Placeholder_ConfirmPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на E-mail.
        /// </summary>
        public static string Placeholder_Email {
            get {
                return ResourceManager.GetString("Placeholder_Email", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Login.
        /// </summary>
        public static string Placeholder_Login {
            get {
                return ResourceManager.GetString("Placeholder_Login", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Login or e-mail.
        /// </summary>
        public static string Placeholder_LoginOrEmail {
            get {
                return ResourceManager.GetString("Placeholder_LoginOrEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Password.
        /// </summary>
        public static string Placeholder_Password {
            get {
                return ResourceManager.GetString("Placeholder_Password", resourceCulture);
            }
        }
    }
}
