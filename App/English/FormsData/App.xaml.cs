
using System;
using System.Diagnostics;
using App.FormsData.Controllers;
using App.FormsData.Init;
using App.Framework;
using App.LocalData;
using App.Localization;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace App.English.FormsData
{
    public partial class EnglishApp : Application
    {
        public EnglishApp()
        {
            InitializeComponent();

            LocalStorage.Init();

            AppFramework.Init();

            InitController.Init();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
