﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace App.Framework.Tools
{
    public static class ColorTools
    {
        public static Color Lerp(Color a, Color b, double t)
        {
            return new Color(a.R + (b.R - a.R) * t, a.G + (b.G - a.G) * t, a.B + (b.B - a.B) * t);
        }
    }
}
