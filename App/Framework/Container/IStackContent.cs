﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Framework
{
    public interface IStackContent
    {
        void OnAdd();
        void OnRemove();

        void OnCurrent();
        void OnPrevious();
    }
}
