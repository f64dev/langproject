﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App.Framework
{
    public class RelativeStackContainerLayout : RelativeLayout
    {
        #region Init

        public List<View> ListView { get; private set; }



        public View CurrentElement { get; private set; }
        public View PreviousElement { get; private set; }


        public RelativeStackContainerLayout()
        {
            ListView = new List<View>();
        }

        public RelativeStackContainerLayout(View view)
        {
            ListView = new List<View>();

            Add(view);
        }


        #endregion
        #region Animation

        private const string namePushAnimation = "Push";
        private const string namePopAnimation = "Pop";

        public Animation PushAnimation { get; set; }
        public Animation PopAnimation { get; set; }

        #endregion
        #region Methods


        public void Set(View view, uint length = 250, Action start = null, Action end = null)
        {
            if (view == null)
                return;

            if (this.AnimationIsRunning(namePushAnimation) || this.AnimationIsRunning(namePopAnimation))
                return;

            view.TranslationX = 99999999;
            view.TranslationY = 99999999;


            Add(view);

            start?.Invoke();

            Action<double, bool> finished = (d, b) =>
            {
                foreach (var item in ListView)
                {
                    if (item != CurrentElement)
                        (item as IStackContent)?.OnRemove();
                }

                Children.Clear();
                this.ListView.Clear();

                Children.Add
                (
                    view,
                    heightConstraint: Constraint.RelativeToParent((p) => p.Height),
                    widthConstraint: Constraint.RelativeToParent((p) => p.Width),
                    yConstraint: Constraint.Constant(0),
                    xConstraint: Constraint.Constant(0)
                );

                this.ListView.Add(CurrentElement);

                PreviousElement = null;

                end?.Invoke();
            };


            if (PushAnimation != null)
            {
                PushAnimation.Commit(this, namePushAnimation, length: length, finished: finished);
            }
            else
            {
                view.TranslationX = 0;
                view.TranslationY = 0;

                finished.Invoke(0, true);
            }
        }


        public void Push(View view, uint length = 250, Action start = null, Action end = null)
        {
            if (view == null)
                return;

            if (this.AnimationIsRunning(namePushAnimation) || this.AnimationIsRunning(namePopAnimation))
                return;

            view.TranslationX = 99999999;
            view.TranslationY = 99999999;


            Add(view);

            start?.Invoke();

            Action<double, bool> finished = (d, b) =>
            {
                Children.Clear();

                Children.Add
                (
                    view,
                    heightConstraint: Constraint.RelativeToParent((p) => p.Height),
                    widthConstraint: Constraint.RelativeToParent((p) => p.Width),
                    yConstraint: Constraint.Constant(0),
                    xConstraint: Constraint.Constant(0)
                );

                end?.Invoke();
            };


            if (PushAnimation != null)
            {
                PushAnimation.Commit(this, namePushAnimation, length: length, finished: finished);
            }
            else
            {
                view.TranslationX = 0;
                view.TranslationY = 0;

                finished.Invoke(0, true);
            }
        }


        public void Pop(uint length = 250, Action start = null, Action end = null)
        {
            if (this.ListView.Count == 0)
                return;

            CurrentElement = ListView[ListView.Count - 1];
            PreviousElement = ListView.Count == 1 ? null : ListView[ListView.Count - 2];



            var curr = CurrentElement as IStackContent;
            

            Children.Clear();


            if (PreviousElement != null)
            {




                var prev = PreviousElement as IStackContent;
               prev?.OnCurrent();

                Children.Add
                (
                    PreviousElement,
                    heightConstraint: Constraint.RelativeToParent((p) => p.Height),
                    widthConstraint: Constraint.RelativeToParent((p) => p.Width),
                    yConstraint: Constraint.Constant(0),
                    xConstraint: Constraint.Constant(0)
                );

                PreviousElement.TranslationX = 99999999;
                PreviousElement.TranslationY = 99999999;
            }

            Children.Add
            (
                CurrentElement,
                heightConstraint: Constraint.RelativeToParent((p) => p.Height),
                widthConstraint: Constraint.RelativeToParent((p) => p.Width),
                yConstraint: Constraint.Constant(0),
                xConstraint: Constraint.Constant(0)
            );

            start?.Invoke();

            Action<double, bool> finished = (d, b) =>
            {
              

                Children.Clear();

                if (curr != null)
                    curr?.OnRemove();

                ListView.RemoveAt(ListView.Count-1);

                CurrentElement = PreviousElement;

                if(CurrentElement!=null)
                    Children.Add
                    (
                        CurrentElement,
                        heightConstraint: Constraint.RelativeToParent((p) => p.Height),
                        widthConstraint: Constraint.RelativeToParent((p) => p.Width),
                        yConstraint: Constraint.Constant(0),
                        xConstraint: Constraint.Constant(0)
                    );


                PreviousElement = ListView.Count <= 1 ? null : ListView[ListView.Count - 2];

                end?.Invoke();
            };

            if (PopAnimation != null)
                PopAnimation.Commit(this, namePopAnimation, length: length, finished: finished);
            else
            {
                if (PreviousElement != null)
                {
                    PreviousElement.TranslationX = 0;
                    PreviousElement.TranslationY = 0;
                }

                finished.Invoke(0, true);
            }


        }



        public void Add(View view)
        {
            Children.Clear();


            PreviousElement = ListView.Count != 0 ? ListView[ListView.Count-1] : null;
            CurrentElement = view;

            if(PreviousElement!=null)
                Children.Add
                (
                    PreviousElement,
                    heightConstraint: Constraint.RelativeToParent((p) => p.Height),
                    widthConstraint: Constraint.RelativeToParent((p) => p.Width),
                    yConstraint: Constraint.Constant(0),
                    xConstraint: Constraint.Constant(0)
                );

            Children.Add
            (
                CurrentElement,
                heightConstraint: Constraint.RelativeToParent((p) => p.Height),
                widthConstraint: Constraint.RelativeToParent((p) => p.Width),
                yConstraint: Constraint.Constant(0),
                xConstraint: Constraint.Constant(0)
            );

            this.ListView.Add(view);

            var i = view as IStackContent;

            if (i != null)
            {
                i.OnAdd();
                i.OnCurrent();
            }

            if (PreviousElement != null)
                (PreviousElement as IStackContent)?.OnPrevious();
        }

        #endregion
    }
}