﻿using System;
using System.Collections.Generic;
using System.Text;
using Lottie.Forms;
using Xamarin.Forms;

namespace App.Framework.Customs
{
    public class AnimationButton : AbsoluteLayout
    {
        public AnimationButton()
        {
            Button = new Button();

            AnimationView = new AnimationView() { IsEnabled = false ,IsVisible=false};

            Children.Add(Button, new Rectangle(1, 1, 1, 1), AbsoluteLayoutFlags.All);

            Children.Add(AnimationView, new Rectangle(1, 1, 1, 1), AbsoluteLayoutFlags.All);


        }

        public Button Button { get; private set; }

        public AnimationView AnimationView { get; private set; }


    }
}
