﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace App.Framework
{
    public class BarLayout
    {
        public BarButtonType ButtonL1 { get; set; }
        public BarButtonType ButtonL2 { get; set; }

        public BarButtonType ButtonR1 { get; set; }
        public BarButtonType ButtonR2 { get; set; }



        public Color BackgroundColor { get; set; } = Color.FromHex("#1976D2");

        private View contentView;
        public View ContentView
        {
            get => contentView;
            set
            {
                contentView = value;
            }
        }
    }
}
