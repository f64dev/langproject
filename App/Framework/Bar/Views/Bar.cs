﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace App.Framework
{
    public class Bar : RelativeLayout
    {
        #region Init

        public BarButtonView ButtonL1 { get; private set; }
        public BarButtonView ButtonL2 { get; private set; }
        public BarButtonView ButtonR1 { get; private set; }
        public BarButtonView ButtonR2 { get; private set; }

        public View Content { get; private set; }
        public T GetContent<T>() where T : View => Content as T;

        public Bar()
        {
            ButtonL1 = new BarButtonView();
            ButtonL2 = new BarButtonView();
            ButtonR1 = new BarButtonView();
            ButtonR2 = new BarButtonView();




            Children.Add
           (
              view: ButtonL1,
              heightConstraint: Constraint.RelativeToParent((p) => p.Height),
              widthConstraint: Constraint.RelativeToParent((p) => p.Height),
              xConstraint: Constraint.Constant(0),
              yConstraint: Constraint.Constant(0)
           );


            Children.Add
           (
              view: ButtonL2,
              heightConstraint: Constraint.RelativeToParent((p) => p.Height),
              widthConstraint: Constraint.RelativeToParent((p) => p.Height),
              xConstraint: Constraint.RelativeToParent((p) => p.Height),
              yConstraint: Constraint.Constant(0)
           );

            Children.Add
           (
              view: ButtonR1,
              heightConstraint: Constraint.RelativeToParent((p) => p.Height),
              widthConstraint: Constraint.RelativeToParent((p) => p.Height),
              xConstraint: Constraint.RelativeToParent((p) => p.Width - p.Height),
              yConstraint: Constraint.Constant(0)
           );

          
            Children.Add
           (
              view: ButtonR2,
              heightConstraint: Constraint.RelativeToParent((p) => p.Height),
              widthConstraint: Constraint.RelativeToParent((p) => p.Height),
              xConstraint: Constraint.RelativeToParent((p) => p.Width - p.Height * 2),
              yConstraint: Constraint.Constant(0)
           );




        }

        #endregion
        #region Layout

        private BarLayout currentLayout;
        public BarLayout CurrentLayout
        {
            get =>currentLayout;
            set
            {
               // var oldLayout = currentLayout;
                currentLayout = value;


                if (Content != null)
                    Children.Remove(Content);

                if (value.ContentView != null)
                {

                    Content = value.ContentView;


                    Children.Add
                    (
                      view: Content,
                      heightConstraint: Constraint.RelativeToParent((p) => p.Height),
                      widthConstraint: Constraint.RelativeToParent((p) =>
                      {
                          var w = p.Width;

                          if (value.ButtonL1 != BarButtonType.None && value.ButtonL2 != BarButtonType.None)
                              w -= p.Height * 2;
                          else if (value.ButtonL1 != BarButtonType.None)
                              w -= p.Height;


                          if (value.ButtonR1 != BarButtonType.None && value.ButtonR2 != BarButtonType.None)
                              w -= p.Height * 2;
                          else if (value.ButtonR1 != BarButtonType.None)
                              w -= p.Height;


                          return w;
                      }),
                      xConstraint: Constraint.RelativeToParent((p) =>
                      {
                          var x = 0d;

                          if (value.ButtonL1 != BarButtonType.None && value.ButtonL2 != BarButtonType.None)
                              x += p.Height * 2;
                          else if (value.ButtonL1 != BarButtonType.None)
                              x += p.Height;

                          return x;
                      }),
                      yConstraint: Constraint.Constant(0)
                    );
                }






                if (value.ButtonL1 != BarButtonType.None)
                {
                   // ButtonL1.Clear();
                    ButtonL1.Animation = BarButtons.GetAnimation(value.ButtonL1);


                    ButtonL1.IsEnabled = ButtonL1.IsVisible = true;
                }
                else
                {
                    ButtonL1.IsEnabled = ButtonL1.IsVisible = false;
                }


                if (value.ButtonL2 != BarButtonType.None)
                {
                   // ButtonL2.Clear();
                    ButtonL2.Animation = BarButtons.GetAnimation(value.ButtonL2);

                    ButtonL2.IsEnabled = ButtonL2.IsVisible = true;
                }
                else
                {
                    ButtonL2.IsEnabled = ButtonL2.IsVisible = false;
                }



                if (value.ButtonR1 != BarButtonType.None)
                {
                   // ButtonR1.Clear();
                    ButtonR1.Animation = BarButtons.GetAnimation(value.ButtonR1);

                    ButtonR1.IsEnabled = ButtonR1.IsVisible = true;
                }
                else
                {
                    ButtonR1.IsEnabled = ButtonR1.IsVisible = false;
                }

                if (value.ButtonR2 != BarButtonType.None)
                {
//ButtonR2.Clear();
                    ButtonR2.Animation = BarButtons.GetAnimation(value.ButtonR2);

                    ButtonR2.IsEnabled = ButtonR2.IsVisible = true;
                }
                else
                {
                    ButtonR2.IsEnabled = ButtonR2.IsVisible = false;
                }














                //Children.Clear();

                //if (value.ContentView != null)
                //{

                //    Content = value.ContentView;


                //    Children.Add
                //    (
                //      view: Content,
                //      heightConstraint: Constraint.RelativeToParent((p) => p.Height),
                //      widthConstraint: Constraint.RelativeToParent((p) =>
                //      {
                //          var w = p.Width;

                //          if (value.ButtonL1 != BarButtonType.None && value.ButtonL2 != BarButtonType.None)
                //              w -= p.Height * 2;
                //          else if (value.ButtonL1 != BarButtonType.None)
                //              w -= p.Height;


                //          if (value.ButtonR1 != BarButtonType.None && value.ButtonR2 != BarButtonType.None)
                //              w -= p.Height * 2;
                //          else if (value.ButtonR1 != BarButtonType.None)
                //              w -= p.Height;


                //          return w;
                //      }),
                //      xConstraint: Constraint.RelativeToParent((p) =>
                //      {
                //          var x = 0d;

                //          if (value.ButtonL1 != BarButtonType.None && value.ButtonL2 != BarButtonType.None)
                //              x += p.Height * 2;
                //          else if (value.ButtonL1 != BarButtonType.None)
                //              x += p.Height;

                //          return x;
                //      }),
                //      yConstraint: Constraint.Constant(0)
                //    );
                //}






                //if (value.ButtonL1 != BarButtonType.None)
                //{
                //    ButtonL1.Clear();
                //    ButtonL1.Animation = BarButtons.GetAnimation(value.ButtonL1);

                //    Children.Add
                //   (
                //      view: ButtonL1,
                //      heightConstraint: Constraint.RelativeToParent((p) => p.Height),
                //      widthConstraint: Constraint.RelativeToParent((p) => p.Height),
                //      xConstraint: Constraint.Constant(0),
                //      yConstraint: Constraint.Constant(0)
                //   );
                //}
                //if (value.ButtonL2 != BarButtonType.None)
                //{
                //    ButtonL2.Clear();
                //    ButtonL2.Animation = BarButtons.GetAnimation(value.ButtonL2);

                //    Children.Add
                //   (
                //      view: ButtonL2,
                //      heightConstraint: Constraint.RelativeToParent((p) => p.Height),
                //      widthConstraint: Constraint.RelativeToParent((p) => p.Height),
                //      xConstraint: Constraint.RelativeToParent((p) => p.Height),
                //      yConstraint: Constraint.Constant(0)
                //   );
                //}



                //if (value.ButtonR1 != BarButtonType.None)
                //{
                //    ButtonR1.Clear();
                //    ButtonR1.Animation = BarButtons.GetAnimation(value.ButtonR1);

                //    Children.Add
                //   (
                //      view: ButtonR1,
                //      heightConstraint: Constraint.RelativeToParent((p) => p.Height),
                //      widthConstraint: Constraint.RelativeToParent((p) => p.Height),
                //      xConstraint: Constraint.RelativeToParent((p) => p.Width - p.Height),
                //      yConstraint: Constraint.Constant(0)
                //   );
                //}
                //if (value.ButtonR2 != BarButtonType.None)
                //{
                //    ButtonR2.Clear();
                //    ButtonR2.Animation = BarButtons.GetAnimation(value.ButtonR2);

                //    Children.Add
                //   (
                //      view: ButtonR2,
                //      heightConstraint: Constraint.RelativeToParent((p) => p.Height),
                //      widthConstraint: Constraint.RelativeToParent((p) => p.Height),
                //      xConstraint: Constraint.RelativeToParent((p) => p.Width - p.Height * 2),
                //      yConstraint: Constraint.Constant(0)
                //   );
                //}
            }
        }

        #endregion
    }
}