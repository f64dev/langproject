﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lottie.Forms;
using Xamarin.Forms;

namespace App.Framework
{
    public class BarButtonView : AnimationView
    {
        #region Init

        public BarButtonView()
        {
            Margin = new Thickness(8);
        }

        public void Clear()
        {
            Pause();
            Animation = null;
        }

        #endregion
        #region Animation


        public BarButtonType buttonType;


        private string menuClose = "Animations/MenuToClose.json";
        private string backMenu = "Animations/BackToMenu.json";




        public void PlayOpenMenuButtonAnim()
        {
            Animation = menuClose;

            PlayProgressSegment(0.15f, 0.5f);
        }

        public void PlayCloseMenuButtonAnim()
        {
            Animation = menuClose;

            PlayProgressSegment(0.6f, 1f);
        }




        public void PlayMenuToBack()
        {
            Animation = backMenu;


            PlayProgressSegment(0.6f, 1f);
        }

        public void PlayBackToMenu()
        {
            Animation = backMenu;
            PlayProgressSegment(0.0f, 0.5f);
        }

        #endregion
        #region Set

        public void SetMenuButton()
        {
            Animation = menuClose;
        }


        public void SetCloseButton()
        {
            
          
            Animation = "Animations/menuButton2.json";

            Speed = 2.5f;

            // Play();
            //  Progress = 0.4f;
            // Pause();

            //PlayProgressSegment(0.4f, 0.5f);



         //   Device.BeginInvokeOnMainThread(() => PlayProgressSegment(0f, 1f));


        }

        public void SetCloseButton2()
        {


            Animation = "Animations/data.json";

            Speed = 2.5f;

            // Play();
            //  Progress = 0.4f;
            // Pause();

            //PlayProgressSegment(0.4f, 0.5f);



            //   Device.BeginInvokeOnMainThread(() => PlayProgressSegment(0f, 1f));


        }

        #endregion
    }
}