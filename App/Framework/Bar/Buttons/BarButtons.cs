﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Framework
{
    public static class BarButtons
    {
        private const string animMenuToBack = "Animations/menuButton2.json";
        private const string animBackToMenu = "Animations/data.json";


        private static Dictionary<BarButtonType, string> animations = new Dictionary< BarButtonType, string>()
        {
            {BarButtonType.Menu,animMenuToBack },
            {BarButtonType.Back,animBackToMenu }
        };


        private static Dictionary<(BarButtonType, BarButtonType), string> transitions=new Dictionary<(BarButtonType, BarButtonType), string>()
        {
            {(BarButtonType.Menu,BarButtonType.Back),animMenuToBack },
            {(BarButtonType.Back,BarButtonType.Menu),animBackToMenu }
        };



        public static string GetAnimation(BarButtonType button)
        {
            if (button == BarButtonType.None)
                return null;

            animations.TryGetValue(button,out var value);

            return value;
        }

        public static string GetAnimation(BarButtonType from, BarButtonType to)
        {
          transitions.TryGetValue((from,to), out var value);

            return value;
        }
    }
}
