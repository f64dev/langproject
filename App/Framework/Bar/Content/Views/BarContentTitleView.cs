﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace App.Framework
{
	public class BarContentTitleView : Label
	{
        #region Init

        public BarContentTitleView ()
		{
            FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label));
            TextColor = Color.White;

            Margin = new Thickness(10,0);

            VerticalTextAlignment = TextAlignment.Center;
            HorizontalTextAlignment = TextAlignment.Start;
        }

        public void Clear()
        {
            Text = null;
        }

        #endregion

        public void ChangeAndFadeText(string newText, uint len)
        {
            this.FadeTo(-1f, len / 2, Easing.CubicInOut).GetAwaiter().OnCompleted(() =>
            {
                Text = newText;
                this.FadeTo(1f, len / 2, Easing.CubicInOut);
            });
        }



    }
}