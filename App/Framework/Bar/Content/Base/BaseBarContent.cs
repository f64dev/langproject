﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace App.Framework
{
   public abstract class BaseBarContent
   {
       public abstract View GetView();
   }
}
