﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Framework
{
    public interface IBarContent
    {
        BarLayout BarLayout { get; }
    }
}
