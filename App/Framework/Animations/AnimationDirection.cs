﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Framework
{
    public enum AnimationDirection
    {
        None,
        Left,
        Top,
        Right,
        Bottom
    }
}
