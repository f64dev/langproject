﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace App.Framework
{
    public class FrameworkAnimation
    {
        public Func<BarLayout,Animation> Bar { get; set; }
        public Func<Animation> Content { get; set; }
        public Func<Animation> Custom { get; set; }
    }
}
