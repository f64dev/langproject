﻿using System;
using System.Collections.Generic;
using System.Text;
using App.Framework.Tools;
using Xamarin.Forms;

namespace App.Framework
{
    public static class AnimationsTools
    {
        #region Content

        public static Animation TopToBottom(Easing easing = null, Action finished = null) => new Animation((t) =>
        {
            var content = AppFramework.Content;
            content.CurrentElement.TranslationY = content.Height * (t - 1);
        }, easing: easing, finished: finished);



        public static Func<Animation> LeftToRight(Easing easing = null, Action finished = null) => () => new Animation((t) =>
        {
            var content = AppFramework.Content;
            content.CurrentElement.TranslationX = content.Width * (t - 1);
            content.PreviousElement.TranslationX = content.Width * t;

            content.CurrentElement.TranslationY = 0;
            content.PreviousElement.TranslationY = 0;

            content.CurrentElement.Opacity = t;
            content.PreviousElement.Opacity = (1 - t);

        }, easing: easing, finished: finished);


        public static Func<Animation> PopToLeft(Easing easing = null, Action finished = null) => () => new Animation((t) =>
        {
            var content = AppFramework.Content;
            content.CurrentElement.TranslationX = content.Width * -t;

            if (content.PreviousElement!=null)
            content.PreviousElement.TranslationX = content.Width * (1-t);

            content.CurrentElement.TranslationY = 0;
            if (content.PreviousElement != null)
                content.PreviousElement.TranslationY = 0;

            content.CurrentElement.Opacity = (1 - t);
            if (content.PreviousElement != null)
                content.PreviousElement.Opacity = t;

        }, easing: easing, finished: finished);


        public static Func<Animation> PopToRight(Easing easing = null, Action finished = null) => () => new Animation((t) =>
        {
            var content = AppFramework.Content;
            content.CurrentElement.TranslationX = content.Width * t;

            if (content.PreviousElement != null)
                content.PreviousElement.TranslationX = content.Width * -(1 - t);

            content.CurrentElement.TranslationY = 0;
            if (content.PreviousElement != null)
                content.PreviousElement.TranslationY = 0;

            content.CurrentElement.Opacity = (1 - t);
            if (content.PreviousElement != null)
                content.PreviousElement.Opacity = t;

        }, easing: easing, finished: finished);




        public static Func<Animation> RightToLeft(Easing easing = null, Action finished = null) => () => new Animation((t) =>
        {
              var content = AppFramework.Content;

              content.CurrentElement.TranslationX = content.Width * (1 - t);
              content.PreviousElement.TranslationX = content.Width * (-t);

        
            content.CurrentElement.TranslationY = 0;
            content.PreviousElement.TranslationY = 0;


            content.CurrentElement.Opacity = t;
              content.PreviousElement.Opacity = (1 - t);


          }, easing: easing, finished: finished);


        public static Func<Animation> BottomToTop(Easing easing = null, Action finished = null) => () => new Animation((t) =>
        {
            var content = AppFramework.Content;
            content.CurrentElement.TranslationY = content.Height * -t;
        }, easing: easing, finished: finished);

        public static Animation BarContentFade(Easing easing = null, Action finished = null) => new Animation((t) =>
        {
           AppFramework.Bar.Content.Opacity = 1 - t;
        }, easing: easing, finished: finished);

        public static Animation BarContentUnFade(Easing easing = null, Action finished = null) => new Animation((t) =>
        {
            AppFramework.Bar.Content.Opacity = t;
        }, easing: easing, finished: finished);


        #endregion
        #region BarButtons

        private static Animation GetBarButtonsAnimation(BarLayout newLayout)
        {
                var bar = AppFramework.Bar;

                var oldLayout = AppFramework.Bar.CurrentLayout;


                var transitionL1 = BarButtons.GetAnimation(oldLayout.ButtonL1, newLayout.ButtonL1);
                var transitionL2 = BarButtons.GetAnimation(oldLayout.ButtonL2, newLayout.ButtonL2);

                var transitionR1 = BarButtons.GetAnimation(oldLayout.ButtonR1, newLayout.ButtonR1);
                var transitionR2 = BarButtons.GetAnimation(oldLayout.ButtonR2, newLayout.ButtonR2);


                var l1 = GetAnimType(oldLayout.ButtonL1, newLayout.ButtonL1, transitionL1);
                var l2 = GetAnimType(oldLayout.ButtonL2, newLayout.ButtonL2, transitionL2);

                var r1 = GetAnimType(oldLayout.ButtonR1, newLayout.ButtonR1, transitionR1);
                var r2 = GetAnimType(oldLayout.ButtonR2, newLayout.ButtonR2, transitionR2);



                if (l1 == AnimType.Anim)
                    bar.ButtonL1.Animation = transitionL1;

                if (l2 == AnimType.Anim)
                    bar.ButtonL2.Animation = transitionL2;

                if (r1 == AnimType.Anim)
                    bar.ButtonR1.Animation = transitionR1;

                if (r2 == AnimType.Anim)
                    bar.ButtonR2.Animation = transitionR2;


                var anim = new Animation((f) =>
                {
                    if (l1 == AnimType.Anim)
                        bar.ButtonL1.Progress = (float)f;
                    if (l2 == AnimType.Anim)
                        bar.ButtonL2.Progress = (float)f;
                    if (r1 == AnimType.Anim)
                        bar.ButtonR1.Progress = (float)f;
                    if (r2 == AnimType.Anim)
                        bar.ButtonR2.Progress = (float)f;
                });
                anim.Add(0, .5f, new Animation((f) =>
                {
                    if (l1 == AnimType.AnimToAnim)
                    {
                        bar.ButtonL1.Progress = (float)f;
                    }
                    else if (l1 == AnimType.AnimToNull)
                    {
                        bar.ButtonL1.Progress = (float)f;
                        bar.ButtonL1.Opacity = 1-f;
                    }

                    if (l2 == AnimType.AnimToAnim )
                    {
                        bar.ButtonL2.Progress = (float)f;
                    }
                    else if (l2 == AnimType.AnimToNull)
                    {
                        bar.ButtonL2.Progress = (float)f;
                        bar.ButtonL2.Opacity = 1 - f;
                    }

                    if (r1 == AnimType.AnimToAnim)
                    {
                        bar.ButtonR1.Progress = (float)f;
                    }
                    else if (r1 == AnimType.AnimToNull)
                    {
                        bar.ButtonR1.Progress = (float)f;
                        bar.ButtonR1.Opacity = 1 - f;
                    }

                    if (r2 == AnimType.AnimToAnim )
                    {
                        bar.ButtonR2.Progress = (float)f;
                    }
                    else if (r2 == AnimType.AnimToNull)
                    {
                        bar.ButtonR2.Progress = (float)f;
                        bar.ButtonR2.Opacity = 1 - f;
                    }
                },
                finished: () =>
                {
                    AppFramework.Bar.CurrentLayout = newLayout;

                    if (l1 == AnimType.Anim)
                        bar.ButtonL1.Animation = transitionL1;
                    if (l2 == AnimType.Anim)
                        bar.ButtonL2.Animation = transitionL2;
                    if (r1 == AnimType.Anim)
                        bar.ButtonR1.Animation = transitionR1;
                    if (r2 == AnimType.Anim)
                        bar.ButtonR2.Animation = transitionR2;
                }));
                anim.Add(.5f, 1f, new Animation((f) =>
                {
                    if (l1 == AnimType.AnimToAnim)
                    {
                        bar.ButtonL1.Progress = 1 - (float)f;
                    }
                    else if (l1 == AnimType.NullToAnim)
                    {
                        bar.ButtonL1.Progress = 1 - (float)f;
                        bar.ButtonL1.Opacity = f;
                    }

                    if (l2 == AnimType.AnimToAnim)
                    {
                        bar.ButtonL2.Progress = 1 - (float)f;
                    }
                    else if (l2 == AnimType.NullToAnim)
                    {
                        bar.ButtonL2.Progress = 1 - (float)f;
                        bar.ButtonL2.Opacity = f;
                    }

                    if (r1 == AnimType.AnimToAnim)
                    {
                        bar.ButtonR1.Progress = 1 - (float)f;
                    }
                    else if (r1 == AnimType.NullToAnim)
                    {
                        bar.ButtonR1.Progress = 1 - (float)f;
                        bar.ButtonR1.Opacity = f;
                    }

                    if (r2 == AnimType.AnimToAnim)
                    {
                        bar.ButtonR2.Progress = 1 - (float)f;
                    }
                    else if (r2 == AnimType.NullToAnim)
                    {
                        bar.ButtonR2.Progress = 1 - (float)f;
                        bar.ButtonR2.Opacity = f;
                    }
                }));

                return anim;
        }

        private static AnimType GetAnimType(BarButtonType o, BarButtonType n, string transition)
        {
            return o == n
                      ? AnimType.None
                      : transition != null
                           ? AnimType.Anim
                           : n == BarButtonType.None
                               ? AnimType.AnimToNull
                               : o == BarButtonType.None
                                   ? AnimType.NullToAnim
                                   : AnimType.AnimToAnim;
        }

        private enum AnimType
        {
            AnimToNull,
            NullToAnim,
            AnimToAnim,
            Anim,
            None
        }


        public static Func<BarLayout,Animation> BarFade(Easing easing = null,Action finished = null) => (layout) =>
        {
            var bar = AppFramework.Bar;


            var oldColor = bar.CurrentLayout.BackgroundColor;
            var newColor = layout.BackgroundColor;

            var isColorLerp = oldColor != newColor;

            var anim = GetBarButtonsAnimation(layout);
            anim.Add(0, .5f, new Animation((f) =>
            {
              
                bar.Content.Opacity =  1 - f;

               // bar.Content.RotationY=  f;


              //  bar.Content.TranslationY = -bar.Height * f;
            }));
            anim.Add(.5f, 1f, new Animation((f) =>
            {
                 AppFramework.Bar.Content.Opacity = f;

              // bar.Content.TranslationX = bar.Width/2 * (1 - f);
            },easing:Easing.CubicInOut));


            anim.Add(0f, 1f, new Animation((f) =>
            {
                if (isColorLerp)
                    bar.BackgroundColor = ColorTools.Lerp(oldColor, newColor, f);
            }, easing: Easing.CubicInOut));


            return anim;
        };


        #endregion
    }
}
