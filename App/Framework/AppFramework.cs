﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Lottie.Forms;
using Xamarin.Forms;

namespace App.Framework
{
    public static partial class AppFramework
    {
        #region Init

        public static void Init()
        {
            InitPage();
            InitControlInput();

            Application.Current.MainPage = Page;
        }

        public static void Disable()
        {
            ReleaseControlInput();


            PageLayout = null;
            Application.Current.MainPage = Page = null;
        }

        #endregion
        #region Page

        public static ContentPage Page { get; private set; }
        public static RelativeLayout PageLayout { get; private set; }



        public static double BarSize { get; private set; }


        private static void InitPage()
        {
            BarSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 2f;

            Page = new ContentPage();
            Page.Content = PageLayout = new RelativeLayout();



            SetLayout(PageLayoutType.Content);
        }

        #endregion
        #region Content


        private static BarLayout loadingBarLayout = new BarLayout()
        {
          
            ContentView = new AnimationView()
            {
                Animation = "Animations/Loading3.json"
            }
        };


        public static void RestoreBarLayout(FrameworkAnimation animation, BarLayout barLayout)
        {
            EnableInput = false;

            var anim = new Animation();


            Layout = PageLayoutType.BarAndContent;

            if (animation.Bar != null)
                anim.Add(0, 1, animation.Bar(barLayout));


            anim.Commit(Bar, "Restore", finished: (a, asw) => { EnableInput = true; });
        }



        public static void Loading(FrameworkAnimation animation, Func<bool> loading, Func<bool> success = null, Func<bool> fail = null)
        {
            EnableInput = false;



            var oldLayout = Bar.CurrentLayout;

            var task = Task.Run(loading);

            var anim = new Animation();




            Layout = PageLayoutType.BarAndContent;

            if (animation.Bar != null)
                anim.Add(0, 1, animation.Bar(loadingBarLayout));

            if (animation.Custom != null)
                anim.Add(0, 1, animation.Custom());




            var iter = 0;

            var animLoading = new Animation((f) => { Bar.GetContent<AnimationView>().Progress = (float)f; });


            anim.Add(0.5, 0.51, new Animation((f) => { }, finished: () =>
            {
                animLoading.Commit(Bar, "LoadingAnimation", length: 500, repeat: () =>
                {
                    if (iter<1 || !task.IsCompleted)
                    {
                        iter++;
                        return true;
                    }

                    EnableInput = true;

                    var isFinish = false;
                    var result = task.Result;

                    if (result)
                    {
                        if (success != null)
                            isFinish = success.Invoke();
                    }
                    else
                    {
                        if (fail != null)
                            isFinish = fail.Invoke();
                    }

                    if (!isFinish)
                        RestoreBarLayout(animation, oldLayout);


                    return false;
                });
            }));


            anim.Commit(Bar,"BarAnimation",length:500);
        }








        public static void SetContent(View view)
        {
            var barContent = view as IBarContent;

            if (barContent != null)
            {
                var barLayout = barContent.BarLayout;

                if (barLayout == null)
                {
                    Layout = PageLayoutType.Content;
                }
                else
                {
                    Layout = PageLayoutType.BarAndContent;
                    Bar.CurrentLayout = barLayout;
                    Bar.BackgroundColor = barLayout.BackgroundColor;
                }
            }

            Content.PushAnimation = null;


            Content.Set(view);
        }


        public static void PushContent(FrameworkAnimation animation, View view, bool isClear = false, uint length = 500, Action start = null, Action end = null)
        {

            EnableInput = false;
            var anim = new Animation();


            var barContent = view as IBarContent;

            if (barContent != null)
            {
                var barLayout = barContent.BarLayout;

                if (barLayout == null)
                {
                    Layout = PageLayoutType.Content;
                }
                else
                {
                    Layout = PageLayoutType.BarAndContent;

                    if (animation.Bar != null)
                        anim.Add(0, 1, animation.Bar(barLayout));
                }
            }


            if (animation.Content != null)
                anim.Add(0, 1, animation.Content());


            if (animation.Custom != null)
                anim.Add(0, 1, animation.Custom());




            Content.PushAnimation = anim;

            if (isClear)
                Content.Set(view, length, start, () => { end?.Invoke(); EnableInput = true; });
            else
                Content.Push(view, length, start, () => { end?.Invoke(); EnableInput = true; });
        }

        public static void Pop(FrameworkAnimation animation, uint length = 500, Action start = null, Action end = null)
        {
            EnableInput = false;
            var anim = new Animation();



            var barContent = Content.ListView.Count <= 1 ? null : Content.ListView[Content.ListView.Count - 2] as IBarContent;

            if (barContent != null)
            {
                var barLayout = barContent.BarLayout;

                if (barLayout == null)
                {
                    Layout = PageLayoutType.Content;
                }
                else
                {
                    Layout = PageLayoutType.BarAndContent;

                    if (animation.Bar != null)
                        anim.Add(0, 1, animation.Bar(barLayout));
                }
            }


            if (animation.Content != null)
                anim.Add(0, 1, animation.Content());


            if (animation.Custom != null)
                anim.Add(0, 1, animation.Custom());


            Content.PopAnimation = anim;
            Content.Pop(length, start, () => { end?.Invoke(); EnableInput = true; });
        }

        #endregion
        #region Layout 


        public static Bar Bar { get; private set; }
        public static ContentContainer Content { get; private set; }


        private static PageLayoutType layout;
        public static PageLayoutType Layout
        {
            get => layout;
            set
            {
                if (layout == value)
                    return;

                SetLayout(value);
            }
        }

        public static void SetLayout(PageLayoutType l)
        {
            layout = l;

            PageLayout.Children.Clear();

            if (l == PageLayoutType.BarAndContent)
            {
                if (Bar == null)
                    Bar = new Bar();

                PageLayout.Children.Add
                (
                    view: Bar,
                    heightConstraint: Constraint.RelativeToParent((p) => BarSize),
                    widthConstraint: Constraint.RelativeToParent((p) => p.Width)
                );
            }
            else
            {
                Bar = null;
            }

            if (Content == null)
                Content = new ContentContainer();


            PageLayout.Children.Add
            (
                view: Content,
                heightConstraint: Constraint.RelativeToParent((p) => Bar == null ? p.Height : p.Height - BarSize),
                widthConstraint: Constraint.RelativeToParent((p) => p.Width),
                xConstraint: Constraint.Constant(0),
                yConstraint: Constraint.RelativeToParent((p) => Bar == null ? 0 : BarSize)
            );
        }

        #endregion
        #region Modal 

        public static View ModalView { get; private set; }

        public static void ShowModal(View view)
        {

        }
        public static void RemoveModal()
        {

        }

        #endregion
        #region Input Control

        private static RelativeLayout inputControlContainer;
        public static void InitControlInput()
        {
            inputControlContainer = new RelativeLayout();
            inputControlContainer.Opacity = 0;
        }

        public static void ReleaseControlInput()
        {
            inputControlContainer = null;
        }

        private static bool enableInput;
        public static bool EnableInput
        {
            set
            {
                if (enableInput == value)
                    return;



                enableInput = value;

                if (PageLayout.Children.Contains(inputControlContainer))
                    PageLayout.Children.Remove(inputControlContainer);


                if (!value)
                {
                    PageLayout.Children.Add
                    (
                        view: inputControlContainer,
                        widthConstraint: Constraint.RelativeToParent((p) => p.Width),
                        heightConstraint: Constraint.RelativeToParent((p) => p.Height),
                        xConstraint: Constraint.Constant(0),
                        yConstraint: Constraint.Constant(0));
                }
            }
            get => enableInput;
        }

        #endregion
    }
}
