﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PCLStorage;

namespace App.LocalData
{
    public class LocalStorage
    {
        #region Init

        public static void Init()
        {
            InitEmbeddedResource();
            InitLocalResource();
        }

        #endregion
        #region EmbeddedResource

        static string assemblyName;
        static Assembly assembly;
        static HashSet<string> resourceSet;

        private static void InitEmbeddedResource()
        {
            assembly = Assembly.GetAssembly(typeof(LocalStorage));
            assemblyName = assembly.GetName().Name;

            var assemblyLength = assemblyName.Length + 1;

            var resourcePaths = assembly.GetManifestResourceNames();

            for (int i = 0; i < resourcePaths.Length; i++)
                resourcePaths[i] = resourcePaths[i].Remove(0, assemblyLength);

            resourceSet = new HashSet<string>(resourcePaths);
        }




        public static string[] GetAllPathEmbeddedResource(string path)
        {
            var list = new List<string>();

            foreach (var fullPath in resourceSet)
                if (fullPath.StartsWith(path))
                    list.Add(fullPath);


            return list.ToArray();
        }

        public static Stream GetEmbeddedResourceStream(string resourceFileName)
        {
            if (!resourceSet.Contains(resourceFileName))
                return null;

            return assembly.GetManifestResourceStream($"{assemblyName}.{resourceFileName}");
        }

        public static byte[] GetEmbeddedResourceBytes(string resourceFileName)
        {
            var stream = GetEmbeddedResourceStream(resourceFileName);

            if (stream == null)
                return null;

            using (var memoryStream = new MemoryStream())
            {
                stream.CopyTo(memoryStream);
                return memoryStream.ToArray();
            }
        }

        public static string GetEmbeddedResourceString(string resourceFileName)
        {
            var stream = GetEmbeddedResourceStream(resourceFileName);

            if (stream == null)
                return null;

            using (var streamReader = new StreamReader(stream))
            {
                return streamReader.ReadToEnd();
            }
        }

        #endregion
        #region LocalResource

        private static IFolder rootFolder;

        private static void InitLocalResource()
        {
            rootFolder = FileSystem.Current.LocalStorage;
        }


        private static async Task<IFile> GetLocalFile(string path)
        {
            IFile file = null;

            var str = path.Split('/');

            if (str.Length == 1)
            {
                try
                {
                    file = await rootFolder.GetFileAsync(path);
                }
                catch (PCLStorage.Exceptions.FileNotFoundException)
                {
                    file = null;
                }
            }

            if (file != null)
                return file;

            IFolder folder = rootFolder;

            for (int i = 0; i < str.Length - 1; i++)
            {
                try
                {
                    folder = await folder.GetFolderAsync(str[i]);
                }
                catch (PCLStorage.Exceptions.DirectoryNotFoundException)
                {
                    folder = null;
                    break;
                }
            }

            if (folder != null)
            {
                try
                {
                    file = await folder.GetFileAsync(str[str.Length - 1]);
                }
                catch (PCLStorage.Exceptions.FileNotFoundException)
                {
                    file = null;
                }
            }

            return file;
        }

        private static async Task<IFile> CreateLocalFile(string path, string content)
        {
            IFile file = null;

            var str = path.Split('/');

            if (str.Length == 1)
            {
                file = await rootFolder.CreateFileAsync(path, CreationCollisionOption.ReplaceExisting);

                await file.WriteAllTextAsync(content);
            }

            if (file != null)
                return file;

            IFolder folder = rootFolder;

            for (int i = 0; i < str.Length - 1; i++)
            {
                folder = await folder.CreateFolderAsync(str[i], CreationCollisionOption.OpenIfExists);
            }

            if (folder != null)
            {
                file = await folder.CreateFileAsync(str[str.Length - 1], CreationCollisionOption.ReplaceExisting);

                await file.WriteAllTextAsync(content);
            }

            return file;
        }


        public static async Task<Stream> GetFileStream(string name)
        {
            var file = await GetLocalFile(name);

            if (file != null)
                return await file.OpenAsync(PCLStorage.FileAccess.Read);

            return null;
        }

        public static async Task<string> GetFileString(string name)
        {
            var file = await GetLocalFile(name);

            if (file != null)
                return await file.ReadAllTextAsync();

            return null;
        }


        #endregion
        #region Content

        private const string nameAudioFolder = "Audio";
        private const string nameImageFolder = "Image";

        public static async Task<Stream> GetAudioStream(string name)
            => await GetFileStream($"{nameAudioFolder}/{name}");


        public static async Task<Stream> GetImageStream(string name)
            => await GetFileStream($"{nameImageFolder}/{name}");

        #endregion
        #region Modules

        #region Directory

        public static async Task<Data.Modules.Directory.Structure> GetDirectoryStructure()
        {
            var path = $"{Data.Modules.ModulesUtility.nameLocalFolderRoot}/{Data.Modules.Directory.DirectoryUtility.nameLocalFolderRoot}/{Data.Modules.Directory.DirectoryUtility.nameStructureFile}.json";

            var json = await GetFileString(path);

            if (json == null)
                return null;

            var structure = JsonConvert.DeserializeObject<Data.Modules.Directory.Structure>(json);

            structure.Init();

            return structure;
        }


        public static async Task<Data.Modules.Directory.Unit> GetDirectoryUnit(Data.Modules.Directory.StructureElement element)
        {
            return await GetDirectoryUnit(element.path);
        }

        public static async Task<Data.Modules.Directory.Unit> GetDirectoryUnit(string path)
        {
            path = $"{Data.Modules.ModulesUtility.nameLocalFolderRoot}/{Data.Modules.Directory.DirectoryUtility.nameLocalFolderRoot}/{path}.json";

            var json = await GetFileString(path);

            var unit = JsonConvert.DeserializeObject<Data.Modules.Directory.Unit>(json);

            return unit;
        }


        #endregion
        #region Learning

        public static async Task<Data.Modules.Learning.Levels> GetLearningLevels()
        {
            var path = $"{Data.Modules.ModulesUtility.nameLocalFolderRoot}/{Data.Modules.Learning.LearningUtility.nameLocalFolderRoot}/{Data.Modules.Learning.LearningUtility.nameLevelsFile}.json";

            var json = await GetFileString(path);

            if (json == null)
                return null;

            var levels = JsonConvert.DeserializeObject<Data.Modules.Learning.Levels>(json);

            return levels;
        }


        public static async Task<Data.Modules.Learning.Timeline> GetLearningLevel(Data.Modules.Learning.Level level)=> await GetLearningLevel(level.folder);
        
        public static async Task<Data.Modules.Learning.Timeline> GetLearningLevel(string level)
        {
            var path = $"{Data.Modules.ModulesUtility.nameLocalFolderRoot}/{Data.Modules.Learning.LearningUtility.nameLocalFolderRoot}/{level}/{Data.Modules.Learning.LearningUtility.nameLevelFile}.json";

            var json = await GetFileString(path);

            if (json == null)
                return null;

            var timeline = JsonConvert.DeserializeObject<Data.Modules.Learning.Timeline>(json);

            return timeline;
        }

        public static async Task<Data.Modules.Learning.Exercise> GetLearningExercise(string path)
        {
           path = $"{Data.Modules.ModulesUtility.nameLocalFolderRoot}/{Data.Modules.Learning.LearningUtility.nameLocalFolderRoot}/{path}.json";

            var json = await GetFileString(path);

            if (json == null)
                return null;

            var exercise = JsonConvert.DeserializeObject<Data.Modules.Learning.Exercise>(json, new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.Auto });

            return exercise;
        }

        #endregion

        #endregion
    }
}
