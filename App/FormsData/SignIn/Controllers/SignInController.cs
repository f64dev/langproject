﻿using System;
using System.Collections.Generic;
using System.Text;
using App.FormsData.Controllers;
using App.Framework;
using App.Localization;
using Xamarin.Forms;

namespace App.FormsData.SignIn
{
    public class SignInController : BaseController<SignInController>
    {
        #region Init

        public override void OnInit()
        {
            signInView = new SignInView();
        }

        public override void OnRelease()
        {

        }

        #endregion
        #region Content

        public SignInView signInView;

        public Action OnCorrect { get; set; }
        public Action OnCancel { get; set; }

        public void Push()
        {
            AppFramework.PushContent
            (
                new FrameworkAnimation()
                {
                    Bar = AnimationsTools.BarFade(Easing.CubicInOut),
                    Content = AnimationsTools.RightToLeft(Easing.CubicInOut)
                },
               signInView
            );
        }

        #endregion
        #region Styles

        public static class Style
        {
            public static Color ButtonColor_SignIn { get; } = Color.Default;

            public static double ButtonHeight_SignIn { get; } = Device.GetNamedSize(NamedSize.Large, typeof(Label)) * 3;

            public static double ButtonFontSize_SignIn { get; } = Device.GetNamedSize(NamedSize.Large, typeof(Label));


            public static double VerticalMargin { get; } = 12;
            public static double HorizontalMargin { get; } = 25;
        }

        #endregion
    }
}
