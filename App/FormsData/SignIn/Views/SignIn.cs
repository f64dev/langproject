﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using App.FormsData.Behaviors;
using App.FormsData.Views.Customs;
using App.Framework;
using App.Localization;
using Xamarin.Forms;

namespace App.FormsData.SignIn
{
    public class SignInView : StackLayout, IStackContent, IBarContent
    {
        #region BarSetting

        public BarLayout barLayout = new BarLayout()
        {
            ButtonR1 = BarButtonType.Menu,
            ContentView = new BarContentTitleView() { Text = AppLocalization.BarTitle_SignIn }
        };
        public BarLayout BarLayout => barLayout;

        #endregion
        #region Init

        public Entry loginEntry;
        public Entry passwordEntry;
        public Button signInButton;

        public SignInView()
        {
            VerticalOptions = new LayoutOptions(LayoutAlignment.Center, true);

            loginEntry = new Entry()
            {
                Placeholder = AppLocalization.Placeholder_LoginOrEmail,
                Margin = new Thickness
                (
                    SignInController.Style.HorizontalMargin,
                    SignInController.Style.VerticalMargin
                )
            };

            passwordEntry = new Entry()
            {
                Placeholder = AppLocalization.Placeholder_Password,
                Margin = new Thickness
                (
                        SignInController.Style.HorizontalMargin,
                    0,
                        SignInController.Style.HorizontalMargin,
                        SignInController.Style.VerticalMargin
                )
            };

            signInButton = new Button()
            {
                Text = AppLocalization.Button_SignIn,
                HeightRequest = SignInController.Style.ButtonHeight_SignIn,
                FontSize = SignInController.Style.ButtonFontSize_SignIn,
                Margin = new Thickness
                (
                    SignInController.Style.HorizontalMargin,
                    SignInController.Style.VerticalMargin * 2,
                    SignInController.Style.HorizontalMargin,
                    SignInController.Style.VerticalMargin
                )
            };


            Children.Add(loginEntry);
            Children.Add(passwordEntry);
            Children.Add(signInButton);
        }

        #endregion
        #region StackContent

        public void OnAdd()
        {
            Behaviors.Add(new ClickAnimationButton(AppFramework.Bar.ButtonR1, (o, e) =>
            {
                SignInController.Get.OnCancel?.Invoke();
            }));
        }

        public void OnCurrent()
        {

        }

        public void OnPrevious()
        {

        }

        public void OnRemove()
        {

        }

        #endregion
    }
}