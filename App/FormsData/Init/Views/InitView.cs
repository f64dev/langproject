﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using App.FormsData.Behaviors;
using App.FormsData.Controllers;
using App.FormsData.Registration;
using App.FormsData.SignIn;
using App.FormsData.Views.Customs;
using App.Framework;
using App.Localization;
using Xamarin.Forms;

namespace App.FormsData.Init
{
    public class InitView : StackLayout, IStackContent,IBarContent
    {
        public InitView()
        {
            VerticalOptions = new LayoutOptions(LayoutAlignment.Center, true);

            var regButton = new Button()
            {
                Text = "Registration",
                HeightRequest = Device.GetNamedSize(NamedSize.Large, typeof(Button)) * 2,
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Button)),
                Margin = new Thickness(25,10)
            };
            var logButton = new Button()
            {
                Text = "Login",
                HeightRequest = Device.GetNamedSize(NamedSize.Large, typeof(Button)) * 2,
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Button)),
                Margin = new Thickness(25, 10)
            };
            var modulesButton = new Button()
            {
                Text = "Modules",
                HeightRequest = Device.GetNamedSize(NamedSize.Large, typeof(Button)) * 2,
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Button)),
                Margin = new Thickness(25, 10)
            };

            Children.Add(regButton);
            Children.Add(logButton);
            Children.Add(modulesButton);

            Behaviors.Add(new ClickButton(regButton,(o,e)=>
            {
                RegistrationController.Init();

                RegistrationController.Get.OnCancel = () => 
                {
                    InitController.Init();
                };


                RegistrationController.Get.Push();

                InitController.Release();
            }));

            Behaviors.Add(new ClickButton(logButton, (o, e) =>
            {
                SignInController.Init();

                SignInController.Get.OnCancel = () =>
                {
                    InitController.Init();
                };


                SignInController.Get.Push();

                InitController.Release();
            }));

        }

        private BarLayout barLayout = new BarLayout()
        {
            ContentView = new BarContentTitleView() {Text= "InitTest" },
            BackgroundColor =Color.BlueViolet
        };
        public BarLayout BarLayout => barLayout;

        public void OnAdd()
        {

        }

        public void OnCurrent()
        {

        }

        public void OnPrevious()
        {

        }

        public void OnRemove()
        {
            Behaviors.Clear();
        }
    }
}