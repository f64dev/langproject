﻿using System;
using System.Collections.Generic;
using System.Text;
using App.FormsData.Controllers;
using App.Framework;
using Xamarin.Forms;

namespace App.FormsData.Init
{
    public class InitController : BaseController<InitController>
    {
        #region Init

        public override void OnInit()
        {
            AppFramework.SetContent(new InitView());
        }

        public override void OnRelease()
        {

        }

        #endregion
    }
}
