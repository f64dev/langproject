﻿using System;
using System.Collections.Generic;
using System.Text;
using App.Data.Modules;
using Xamarin.Forms;
using static App.FormsData.Controllers.ModulesController;

namespace App.FormsData.Services
{
   public static class AppSetting
    {
        #region Get/Set/Save/Default

        public static T GetSetting<T>(string key)
        {
            if (Application.Current.Properties.TryGetValue(key, out var value))
                return (T)value;

            return default;
        }

        public static void SetSetting(string key,object obj)
        {
            if (Application.Current.Properties.ContainsKey(key))
                Application.Current.Properties[key] = obj;
            else
                Application.Current.Properties.Add(key, obj);
        }

        public static void Save()
        {
            Application.Current.SavePropertiesAsync();
        }


        public static void SetDefault()
        {
            Application.Current.Properties.Clear();
            Save();
           // Application.Current.SavePropertiesAsync();
        }

        #endregion
        #region Modules

        private const string keyCurrentModuleType = "MainPageContentIdx";
        public static ModuleType CurrentModuleType
        {
            get => (ModuleType)GetSetting<int>(keyCurrentModuleType);
            set => SetSetting(keyCurrentModuleType, (int)value);
        }

        #endregion
    }
}
