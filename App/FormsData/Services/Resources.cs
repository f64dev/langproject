﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace App.FormsData.Services
{
   public static class ResourcesHelper
    {
        public static string GetDevicePath(string path)
        {
            switch (Device.RuntimePlatform)
            {
                case Device.Android:
                 return   path.Replace('/', '_').ToLower();
                case Device.UWP:
                    return path;
            }

            return null;
        }
    }
}
