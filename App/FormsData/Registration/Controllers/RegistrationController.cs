﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using App.FormsData.Controllers;
using App.Framework;
using App.Framework.Tools;
using App.Localization;
using Lottie.Forms;
using Xamarin.Forms;

namespace App.FormsData.Registration
{
    public class RegistrationController : BaseController<RegistrationController>
    {
        #region Init

        public override void OnInit()
        {
            registration1 = new Registration1();
            registration2 = new Registration2();
            registration3 = new Registration3();
        }

        public override void OnRelease()
        {
        
        }

        #endregion
        #region Registration

        public Registration1 registration1;
        public Registration2 registration2;
        public Registration3 registration3;


        public Action OnCorrect { get; set; }
        public Action OnCancel { get; set; }

        public void Push()
        {
            AppFramework.PushContent
            (
                new FrameworkAnimation()
                {
                    Bar = AnimationsTools.BarFade(Easing.CubicInOut),
                    Content = AnimationsTools.RightToLeft(Easing.CubicInOut)
                }, 
                registration1
            );
        }

        #endregion
        #region Styles

        public static class Style
        {
            public static Color ButtonColor_Next { get; } = Color.Default;

            public static double ButtonHeight_Next { get; } = Device.GetNamedSize(NamedSize.Large, typeof(Button)) * 2;

            public static double ButtonFontSize_Next { get; } = Device.GetNamedSize(NamedSize.Medium, typeof(Button));


            public static double VerticalMargin { get; } = 12;
            public static double HorizontalMargin { get; } = 25;
        }

        #endregion
    }
}
