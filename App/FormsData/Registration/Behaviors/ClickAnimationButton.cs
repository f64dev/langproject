﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using App.FormsData.Controllers;
using App.FormsData.Views.Content;
using App.FormsData.Views.Main;
using App.Framework;
using App.Framework.Customs;
using Lottie.Forms;
using Xamarin.Forms;

namespace App.FormsData.Registration
{
    public class LoadingButton : Behavior<Registration3>
    {
        private Registration3 stack;


        protected override void OnAttachedTo(Registration3 view)
        {
            view.regButton.Clicked += Click;

            stack = view;



            base.OnAttachedTo(view);
        }

        protected override void OnDetachingFrom(Registration3 view)
        {
            view.regButton.Clicked -= Click;

            stack = null;

            base.OnDetachingFrom(view);
        }


        private void Enable()
        {
            stack.regButton.IsEnabled
                = stack.loginEntry.IsEnabled
                = stack.emailEntry.IsEnabled
                = stack.passwordEntry.IsEnabled
                = stack.confirmPasswordEntry.IsEnabled
                = true;
        }
        private void Disable()
        {
            stack.regButton.IsEnabled
                = stack.loginEntry.IsEnabled
                = stack.emailEntry.IsEnabled
                = stack.passwordEntry.IsEnabled
                = stack.confirmPasswordEntry.IsEnabled
                = false;
        }


        private void Click(object sender, EventArgs e)
        {
            Disable();

            AppFramework.Loading
            (
                new FrameworkAnimation()
                {
                    Bar = AnimationsTools.BarFade(Easing.CubicInOut)
                },
                ()=> true,
                ()=>
                {
                    Enable(); 
                    return false;
                },
                () =>
                {
                    Enable();
                    return false;
                });
        }
    }
}
