﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using App.FormsData.Tools;
using App.FormsData.Views.Customs;
using App.Framework;
using App.Framework.Customs;
using App.Localization;
using Lottie.Forms;
using Xamarin.Forms;

namespace App.FormsData.Registration
{
	public class Registration3: StackLayout, IStackContent, IBarContent
    {
        #region BarSetting

        public BarLayout barLayout = new BarLayout()
        {
            ButtonL1 = BarButtonType.Back,
            ButtonR1 = BarButtonType.Menu,
         
             ContentView = new BarContentTitleView() { Text = AppLocalization.BarTitle_Registration3 }
        
    };
        public BarLayout BarLayout => barLayout;

        #endregion
        #region BarSetting

        public Entry loginEntry;
        public Entry emailEntry;

        public Entry passwordEntry;
        public Entry confirmPasswordEntry;

 
        public Button regButton;


        public Registration3()
        {
            VerticalOptions = new LayoutOptions(LayoutAlignment.Center, true);



            loginEntry = new Entry()
            {
                Placeholder = AppLocalization.Placeholder_Login,
                Margin = new Thickness(RegistrationController.Style.HorizontalMargin, RegistrationController.Style.VerticalMargin)
            };

            emailEntry = new Entry()
            {
                Placeholder = AppLocalization.Placeholder_Email,
                Margin = new Thickness(RegistrationController.Style.HorizontalMargin, 0, RegistrationController.Style.HorizontalMargin, RegistrationController.Style.VerticalMargin)
            };


            passwordEntry = new Entry()
            {
                IsPassword = true,
                Placeholder = AppLocalization.Placeholder_Password,
                Margin = new Thickness(RegistrationController.Style.HorizontalMargin, RegistrationController.Style.VerticalMargin)
            };


            confirmPasswordEntry = new Entry()
            {
                IsPassword=true,
                Placeholder = AppLocalization.Placeholder_ConfirmPassword,
                Margin = new Thickness
                (
                    RegistrationController.Style.HorizontalMargin, 
                    0, 
                    RegistrationController.Style.HorizontalMargin,
                    RegistrationController.Style.VerticalMargin
                )
            };


            regButton = new Button()
            {
                Text = AppLocalization.Button_Registration,
                BackgroundColor = RegistrationController.Style.ButtonColor_Next,
                FontSize = RegistrationController.Style.ButtonFontSize_Next,
                HeightRequest = RegistrationController.Style.ButtonHeight_Next,
                Margin = new Thickness(25, 25, 25, 0)
            };

       


            Children.Add(loginEntry);
            Children.Add(emailEntry);

            Children.Add(passwordEntry);
            Children.Add(confirmPasswordEntry);

            Children.Add(regButton);

         
        }

        #endregion
        #region StackContent

        public void OnAdd()
        {
            Behaviors.Add(new LoadingButton());
        }

        public void OnCurrent()
        {

        }

        public void OnPrevious()
        {

        }

        public void OnRemove()
        {
            Behaviors.Clear();
        } 

        #endregion
    }
}