﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using App.FormsData.Behaviors;
using App.FormsData.Tools;
using App.FormsData.Views.Customs;
using App.Framework;
using App.Framework.Tools;
using App.Localization;
using Xamarin.Forms;

namespace App.FormsData.Registration
{
    public class Registration2 : StackLayout, IStackContent, IBarContent
    {
        #region BarSetting

        public BarLayout barLayout = new BarLayout()
        {
            ButtonL1 = BarButtonType.Back,
            ButtonR1 = BarButtonType.Menu,
            ContentView = new BarContentTitleView() { Text = AppLocalization.BarTitle_Registration2 }
        };
        public BarLayout BarLayout => barLayout;

        #endregion
        #region Init

        public DatePicker dobPicker;
        public Picker genderPicker;
        public Button nextButton;

        public Registration2()
        {
            VerticalOptions = new LayoutOptions(LayoutAlignment.Center, true);

            var dobLabel = new Label()
            {
                Text = AppLocalization.Label_DateOfBirth,
                Margin = new Thickness
                (
                    RegistrationController.Style.HorizontalMargin,
                    RegistrationController.Style.VerticalMargin,
                    RegistrationController.Style.HorizontalMargin,
                    0
                )
            };
            var genderLabel = new Label()
            {
                Text = AppLocalization.Label_Gender,
                Margin = new Thickness
           (
               RegistrationController.Style.HorizontalMargin,
               RegistrationController.Style.VerticalMargin,
               RegistrationController.Style.HorizontalMargin,
               0
           )
            };

            dobPicker = new DatePicker()
            {
                Date = new DateTime(),
                Margin = new Thickness
                (
                    RegistrationController.Style.HorizontalMargin,
                    0,
                    RegistrationController.Style.HorizontalMargin,
                    RegistrationController.Style.VerticalMargin
                )
            };
            genderPicker = new Picker()
            {
                Margin = new Thickness
                (
                    RegistrationController.Style.HorizontalMargin,
                    0,
                    RegistrationController.Style.HorizontalMargin,
                    RegistrationController.Style.VerticalMargin
                ),
                Items =
                {
                    AppLocalization.Label_GenderMale,
                    AppLocalization.Label_GenderFemale,
                    AppLocalization.Label_GenderOther
                }
            };

            nextButton = new Button()
            {
                Text = AppLocalization.Button_Next,
                HeightRequest = RegistrationController.Style.ButtonHeight_Next,
                FontSize = RegistrationController.Style.ButtonFontSize_Next,
                Margin = new Thickness
                (
                    RegistrationController.Style.HorizontalMargin,
                    RegistrationController.Style.VerticalMargin * 2,
                    RegistrationController.Style.HorizontalMargin,
                    RegistrationController.Style.VerticalMargin
                )
            };

            Children.Add(dobLabel);
            Children.Add(dobPicker);
            Children.Add(genderLabel);
            Children.Add(genderPicker);
            Children.Add(nextButton);
        }

        #endregion
        #region StackContent

        public void OnAdd()
        {
            Behaviors.Add(new ClickAnimationButton(AppFramework.Bar.ButtonL1, (o, e) =>
            {
                AppFramework.Pop(new FrameworkAnimation()
                {
                    Bar = AnimationsTools.BarFade(Easing.CubicInOut),
                    Content = AnimationsTools.PopToRight(Easing.CubicInOut)
                });
            }));

            Behaviors.Add(new ClickButton(nextButton, (o, e) =>
            {
                AppFramework.PushContent(new FrameworkAnimation()
                {
                    Bar = AnimationsTools.BarFade(Easing.CubicInOut),
                    Content = AnimationsTools.RightToLeft(Easing.CubicInOut)
                }, RegistrationController.Get.registration3);
            }));
        }

        public void OnCurrent()
        {

        }

        public void OnPrevious()
        {

        }

        public void OnRemove()
        {
            Behaviors.Clear();
        }

        #endregion
    }
}