﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using App.FormsData.Behaviors;
using App.FormsData.Controllers;
using App.FormsData.Views.Customs;
using App.Framework;
using App.Framework.Tools;
using App.Localization;
using Xamarin.Forms;

namespace App.FormsData.Registration
{
	public class Registration1 : StackLayout, IStackContent, IBarContent
    {
        #region BarSetting

        public BarLayout barLayout = new BarLayout()
        {
            ButtonR1 = BarButtonType.Menu,
            ContentView = new BarContentTitleView() { Text = AppLocalization.BarTitle_Registration1 }
        };
        public BarLayout BarLayout => barLayout;

        #endregion
        #region Init


        public Entry firstName;
        public Entry lastName;
        public Button nextButton;



        public Registration1()
        {
            VerticalOptions = new LayoutOptions(LayoutAlignment.Center, true);

            firstName = new Entry()
            {
                Placeholder = AppLocalization.FirstNamePlaceholder,
                Margin = new Thickness
                (
                    RegistrationController.Style.HorizontalMargin,
                    RegistrationController.Style.VerticalMargin
                )
            };

            lastName = new Entry()
            {
                Placeholder = AppLocalization.LastNamePlaceholder,
                Margin = new Thickness
                (
                    RegistrationController.Style.HorizontalMargin,
                    0, 
                    RegistrationController.Style.HorizontalMargin,
                    RegistrationController.Style.VerticalMargin
                )
            };

            nextButton = new Button()
            {
                Text = AppLocalization.Button_Next,
                HeightRequest = RegistrationController.Style.ButtonHeight_Next,
                FontSize = RegistrationController.Style.ButtonFontSize_Next,
                Margin = new Thickness
                (
                    RegistrationController.Style.HorizontalMargin,
                    RegistrationController.Style.VerticalMargin*2,
                    RegistrationController.Style.HorizontalMargin,
                    RegistrationController.Style.VerticalMargin
                )
            };


            Children.Add(firstName);
            Children.Add(lastName);
            Children.Add(nextButton);
        }

        #endregion
        #region StackContent

        public void OnAdd()
        {
            Behaviors.Add(new ClickAnimationButton(AppFramework.Bar.ButtonR1,(o,e)=> 
            {
                RegistrationController.Get.OnCancel?.Invoke();
            }));

            Behaviors.Add(new ClickButton(nextButton, (o, e) =>
            {
                AppFramework.PushContent(new FrameworkAnimation()
                {
                    Bar = AnimationsTools.BarFade(Easing.CubicInOut),
                    Content = AnimationsTools.RightToLeft(Easing.CubicInOut)
                }, RegistrationController.Get.registration2);
            }));
        }

        public void OnCurrent()
        {

        }

        public void OnPrevious()
        {

        }

        public void OnRemove()
        {
            Behaviors.Clear();
        }

        #endregion
    }
}