﻿using System.Collections.Generic;
using App.Data.Modules.Directory;
using App.FormsData.Behaviors.Grammar;
using App.FormsData.Behaviors.Main;
using App.FormsData.Controllers.Content;
using App.FormsData.Views.Customs;
using App.Framework;
using Xamarin.Forms;

namespace App.FormsData.Views.Content.Grammar.Structure
{
    public class UnitList : ScrollView, IStackContent
    {
        #region Content

        private string title;
        public string Title => title;

        public void OnAdd()
        {
        //    Debug.WriteLine(GrammarController.Get.GrammarContent.Stack.Count);
            
        }

        public void OnRemove()
        {
            foreach (var b in Elements)
                b.OnDestroy();

            Behaviors.Clear();
        }



        public void OnCurrent()
        {
            var content = GrammarController.Get.GrammarContent;

            if (content.ListView.Count == 1)
                Behaviors.Add(new OpenMenu());
            else

                Behaviors.Add(new BackButton());
        

        }

        public void OnPrevious()
        {
            Behaviors.Clear();
        }


        #endregion
        #region Init

        public List<BaseUnitElement> Elements { get;private set; }


        private double GetHeightButton { get => Device.GetNamedSize(NamedSize.Large,typeof(Button))*3; }

       


        public UnitList(StructureElement element=null)
        {
            BackgroundColor = Color.Aquamarine;

            var stack = new StackLayout();
            stack.Padding = new Thickness(15);

            Content = stack;


       
            var elements= GrammarController.Get.GetAllElementInFolder(element?.path);

            title = element?.name;

            Elements = new List<BaseUnitElement>();



            foreach (var e in elements)
            {
                BaseUnitElement b;

                if (e.IsFolder())
                    b = new FolderElement(e);
                else
                    b = new UnitElement(e);

                Elements.Add(b);
                stack.Children.Add(b);

                b.HeightRequest = GetHeightButton;

                b.OnCreate();
            }
        }

        #endregion
    }
}