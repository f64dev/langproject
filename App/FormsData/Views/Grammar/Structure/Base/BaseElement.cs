﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace App.FormsData.Views.Content.Grammar
{
    public abstract class BaseUnitElement : Button
    {
        public global::App.Data.Modules.Directory.StructureElement element;

        public BaseUnitElement(App.Data.Modules.Directory.StructureElement element)
        {
            this.element= element;
        }

        public virtual void OnCreate()
        {

        }

        public virtual void OnDestroy()
        {
            element = null;
        }

    }
}
