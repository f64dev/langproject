﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using App.Data.Modules.Directory;
using App.FormsData.Behaviors.Grammar;
using App.FormsData.Controllers.Content;
using Xamarin.Forms;

namespace App.FormsData.Views.Content.Grammar.Structure
{
    public class FolderElement : BaseUnitElement
    {
        public override void OnCreate()
        {
            Behaviors.Add(new ClickFolder());
        }

        public override void OnDestroy()
        {
            Behaviors.Clear();
        }


        public FolderElement(StructureElement element) : base(element)
        {
            Text = element.name;
            BackgroundColor = Color.Olive;
        }
    }
}