﻿using App.FormsData.Behaviors.Grammar;

namespace App.FormsData.Views.Content.Grammar.Structure
{
    public class UnitElement : BaseUnitElement
	{
        public UnitElement(App.Data.Modules.Directory.StructureElement element):base(element)
        {
            Text = element.name;
        }

        public override void OnCreate()
        {
            Behaviors.Add(new ClickFile());
        }

        public override void OnDestroy()
        {
            Behaviors.Clear();
        }

    }
}