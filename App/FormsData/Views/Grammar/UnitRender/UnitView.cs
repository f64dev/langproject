﻿using System;
using System.Collections.Generic;
using System.Text;
using App.Data.Modules.Directory;
using App.FormsData.Behaviors.Grammar;
using App.FormsData.Views.Customs;
using App.FormsData.Views.Grammar.UnitRender.Types;
using App.Framework;
using FormsData.Views.Grammar.UnitRender.Types;
using Xamarin.Forms;

namespace App.FormsData.Views.Content.Grammar.UnitRender
{
    public class UnitView : ScrollView, IStackContent
    {
        public UnitView(Unit unit)
        {

            Orientation = ScrollOrientation.Vertical;


            Title = unit.name;

            var stack = new StackLayout();
            Content = stack;

            BackgroundColor = Color.Beige;
            foreach (var e in unit.elements)
                stack.Children.Add(GetTypeView(e));
        }

        public string Title { get; private set; }





        private View GetTypeView(UnitElement e)
        {
            switch (e.type)
            {
                case ElementType.Label:
                    return new Views.Grammar.UnitRender.Types.Label(e.content);
                case ElementType.Text:
                    return new Text(e.content);
                case ElementType.Image:
                    return new EndabbedImage(e.content);
                case ElementType.EndabbedImage:
                    return new EndabbedImage(e.content);
                case ElementType.TextList:
                    return new TextList(e.content);
                case ElementType.TextTable:
                    return new TextTable(e.content);
            }

            return null;
        }


        public void OnAdd()
        {

        }

        public void OnRemove()
        {
            Behaviors.Clear();
        }



        public void OnCurrent()
        {
            Behaviors.Add(new BackButton());
        }

        public void OnPrevious()
        {
            Behaviors.Clear();
        }
    }
}
