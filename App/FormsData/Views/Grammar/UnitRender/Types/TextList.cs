﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace App.FormsData.Views.Grammar.UnitRender.Types
{
    public class TextList : Xamarin.Forms.Label
    {
        static string[] separatingChars = { " | " };


        public TextList(string content)
        {
            Margin = new Thickness(15, 0, 15, 0);
            FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label));


            SetText(content);
        }

        private void SetText(string content)
        {
            var list = content.Split(separatingChars, StringSplitOptions.RemoveEmptyEntries);

            var ft = new FormattedString();

            foreach (var item in list)
            {
              //  ft.Spans.Add(new Span() { Text = "- " });
                ft.Spans.Add(new Span() { Text = $"{item }\n" });
            }

            FormattedText = ft;
        }
    }
}