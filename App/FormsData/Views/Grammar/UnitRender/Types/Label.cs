﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace App.FormsData.Views.Grammar.UnitRender.Types
{
	public class Label : Xamarin.Forms.Label
    {
		public Label (string content)
		{
            Margin=new Thickness(15,0,15,0);
            FontSize = Device.GetNamedSize(NamedSize.Large,typeof(Label));
            FontAttributes = FontAttributes.Bold;

            Text = content;
        }
	}
}