﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace FormsData.Views.Grammar.UnitRender.Types
{
    public class TextTable : Xamarin.Forms.Grid
    {
        public TextTable(string content)
        {
            var fontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label));


            Margin = new Thickness(15, 0, 15, 0);


            var grid = new Grid();


            var column = content.Split(new string[] { " | " },StringSplitOptions.RemoveEmptyEntries);




            var row = column[0].Split(new string[] { " _ " }, StringSplitOptions.RemoveEmptyEntries); 

            foreach (var c in column)
               ColumnDefinitions.Add(new ColumnDefinition() { Width=new GridLength(1,GridUnitType.Auto)} );

            foreach (var c in row)
              RowDefinitions.Add(new RowDefinition() );


            for (int c = 0; c < column.Length; c++)
            {
                var rowContent = column[c].Split(new string[] { " _ " }, StringSplitOptions.RemoveEmptyEntries);
                for (int r = 0; r < rowContent.Length; r++)
                {
                    var con = rowContent[r];
                  Children.Add(new Xamarin.Forms.Label() {Text=con }, r, c);
                }
            }

            ColumnSpacing = 2;
            RowSpacing = 2;

        }


    }
}