﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using App.FormsData.Behaviors.Main;
using App.FormsData.Controllers;
using App.FormsData.Controllers.Content;
using App.FormsData.Services;
using App.FormsData.Views.Content.Grammar.Structure;
using App.FormsData.Views.Customs;
using App.FormsData.Views.Main.Content;
using App.Framework;
using Xamarin.Forms;

namespace App.FormsData.Views.Content.Grammar
{
    public class GrammarContent : Framework.RelativeStackContainerLayout,  IMainContent, IStackContent
    {
        #region Content

        public string Title => "Грамматика";

        public void OnAdd()
        {
           // ModulesController.Get.CurrentContentType = ApplicationPart.Grammar;
        }

        public void OnRemove()
        {
            foreach (var l in ListView)
            {
                var c = l as IStackContent;

                c.OnRemove();
            }

            ListView.Clear();
            Children.Clear();

            GrammarController.Release();
        }

        public void OnCurrent()
        {
            ((UnitList)CurrentElement).OnCurrent();
        }

        public void OnPrevious()
        {
            ((UnitList)CurrentElement).OnPrevious();
        }





        #endregion
        #region Init


        public GrammarContent()
        {
            BackgroundColor = Color.Firebrick;


           GrammarController.Init();
           GrammarController.Get.GrammarContent = this;

           

            var list = new UnitList();

            Add(list);
        }



        #endregion
    }
}