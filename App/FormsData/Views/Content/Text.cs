﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using App.FormsData.Tools;
using Xamarin.Forms;

namespace App.FormsData.Views.Content
{
    public class Text : Xamarin.Forms.Label
    {
        static string[] separatingChars = { "#|", "|#" };

        public virtual double GetSize=> Device.GetNamedSize(NamedSize.Medium, typeof(Label));


        public Text(string content)
        {
            Margin = new Thickness(10, 0, 10, 0);

            FontSize = GetSize;


            SetText(content);
        }

        private void SetText(string content)
        {
            var buttons = content.Split(separatingChars, StringSplitOptions.RemoveEmptyEntries);

            if (buttons.Length == 1&& buttons[0].Length==content.Length)
            {
                Text = buttons[0];
                return;
            }

            var ft = new FormattedString();
            FormattedText = ft;

            foreach (var item in buttons)
               GetSpan(item);



       

        }

        void GetSpan(string str)
        {
            #region Init

            var parts = str.Split('|');

            Span prevSpan = null, span = new Span(), nextSpan = null;

            span.Text = parts[0];

            span.FontSize = GetSize;


            if (parts.Length == 1)
            {
                FormattedText.Spans.Add(span);
                return;
            }
            #endregion
            #region Check


            for (int i = 1; i < parts.Length; i++)
            {
                var p = parts[i];

                if (ContentParser.IsBold(p))
                {
                    span.FontAttributes = span.FontAttributes | FontAttributes.Bold;
                }
                else if (ContentParser.IsTrans(p, out var trans))
                {
                    nextSpan=new Span() { Text = $"[{trans}]", FontAttributes = FontAttributes.Italic, FontSize = GetSize };
                }
                else if (ContentParser.IsAudio(p, out var audio))
                {
                    prevSpan = new Span() { Text = "🔊", FontAttributes = FontAttributes.Bold, FontSize = GetSize };
                }
            }
            #endregion
            #region Add

            if (prevSpan != null)
                FormattedText.Spans.Add(prevSpan);

            FormattedText.Spans.Add(span);

            if (nextSpan != null)
                FormattedText.Spans.Add(nextSpan);

            #endregion
        }
    }
}