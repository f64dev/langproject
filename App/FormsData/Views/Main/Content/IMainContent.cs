﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace App.FormsData.Views.Main.Content
{
    public interface IMainContent
    {
        string Title { get; }
    }
}
