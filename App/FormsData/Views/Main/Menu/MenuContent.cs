﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using App.Data.Modules;
using App.FormsData.Behaviors;
using App.FormsData.Behaviors.Main;
using App.FormsData.Controllers;
using App.FormsData.Services;
using App.FormsData.Tools;
using App.FormsData.Views.Customs;
using App.Framework;
using Xamarin.Forms;

namespace App.FormsData.Views.Main
{
    public class MenuContent : RelativeLayout, IStackContent
    {
        #region Content

        public void OnAdd()
        {

            foreach (var b in Buttons)
                b.Behaviors.Add(new MenuButtonAction());
   

            Behaviors.Add(new CloseMenu());
        }

        public void OnRemove()
        {
            foreach (var b in Buttons)
                b.Behaviors.Clear();

            Behaviors.Clear();
        }

        public void OnCurrent()
        {

        }

        public void OnPrevious()
        {

        }

        #endregion
        #region Init 

        public MenuContent()
        {
            #region RelativeLayout

            BackgroundColor = Color.DimGray;

            #endregion


            InitUser();
            InitButtons();







            #region MyRegion





            //trainingButton.Clicked += (s, e) =>
            //{
            //   // if (MainController.Get.CurrentContentType == Training.TrainingContainer.Idx)
            //  //  {
            // //       MainController.Get.CloseMainPageMenu();
            ////    }
            //    else
            //    {
            //      //  MainController.Get.CurrentContentType = Training.TrainingContainer.Idx;

            // //       MainController.Get.SelectMainPageContent();
            // //   }
            //};
            //grammarButton.Clicked += (s, e) =>
            //{
            //    if (MainController.Get.CurrentContentType == Grammar.GrammarContent.Idx)
            //    {
            //        MainController.Get.CloseMainPageMenu();
            //    }
            //    else
            //    {
            //        MainController.Get.CurrentContentType = Grammar.GrammarContent.Idx;

            //        MainController.Get.SelectMainPageContent();
            //    }
            //};


            //var selectedColor = Color.Coral;

            //if (MainController.Get.CurrentContentType == Training.TrainingContainer.Idx)

            //    trainingButton.BackgroundColor = selectedColor;

            //else if (MainController.Get.CurrentContentType == Grammar.GrammarContent.Idx)

            //    grammarButton.BackgroundColor = selectedColor;



            #endregion


        }

        #endregion
        #region Buttons

        public MenuButton[] Buttons { get; private set; }

        private void InitButtons()
        {

            var size = Device.GetNamedSize(NamedSize.Large, typeof(Button)) * 3;

            Buttons = new MenuButton[2];

            var trainingButton = new MenuButton(ModuleType.Training);
            trainingButton.Text = "Обучение";


            var grammarButton = new MenuButton(ModuleType.Grammar);
            grammarButton.Text = "Грамматика";


            Children.Add(trainingButton,
                heightConstraint: Constraint.Constant(size),
                widthConstraint: Constraint.RelativeToParent(p => p.Width));

            Children.Add(grammarButton,
                heightConstraint: Constraint.Constant(size),
                widthConstraint: Constraint.RelativeToParent(p => p.Width),
                yConstraint: Constraint.RelativeToParent(p => size));


            Buttons = new MenuButton[] { trainingButton,grammarButton};

        }

        #endregion
        #region User

        private void InitUser()
        {
            var user = new RelativeLayout();

            var size = Device.GetNamedSize(NamedSize.Large, typeof(Button)) * 4;

            var avatar = new Image() { Source =  ResourcesHelper.GetDevicePath( "Icons/Avatar.png") };
            var logout = new Image() { Source = ResourcesHelper.GetDevicePath( "Icons/Logout.png") };
            var setting = new Image() { Source = ResourcesHelper.GetDevicePath("Icons/Settings.png") };


            var login = new Label() { FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)), Text = "TestUser" };



            user.Children.Add(avatar, () => new Rectangle(0, 0, user.Height, user.Height));

            user.Children.Add(login, () => new Rectangle(avatar.Width * 1.1f, 0, user.Width, user.Height / 2));



            user.Children.Add(setting, () => new Rectangle(user.Width - user.Height * 0.5f, user.Height * 0.5f, user.Height * .4f, user.Height * .4f));

            user.Children.Add(logout, () => new Rectangle(user.Width - user.Height * 0.5f * 2, user.Height * 0.5f, user.Height * .4f, user.Height * .4f));


            Children.Add(user, () => new Rectangle(0, Height - size, Width, size));

            user.BackgroundColor = Color.RoyalBlue;
        }

        #endregion
    }
}