﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using App.Data.Modules;
using Xamarin.Forms;

namespace App.FormsData.Views.Main
{
	public class MenuButton : Button
	{
        public ModuleType type;


		public MenuButton (ModuleType type)
		{
            this.type = type;
          FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label));
            BackgroundColor = Color.CadetBlue;
            Margin = new Thickness(15, 15, 15, 0);
        }
	}
}