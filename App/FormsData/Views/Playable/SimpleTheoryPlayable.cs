﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using App.Data.Playables;
using App.Data.Playables.Theory;
using App.FormsData.Controllers;
using Xamarin.Forms;

namespace App.FormsData.Views.Playable
{
    public class SimpleTheoryPlayableView : BasePlayableView
    {
        double buttonSize = Device.GetNamedSize(NamedSize.Large,typeof(Button));

        Label text;


        public SimpleTheoryPlayableView(SimpleTheoryPlayable playable) : base(playable)
        {
            text = new Label() { Text = playable.Text, VerticalTextAlignment = TextAlignment.Center, HorizontalTextAlignment = TextAlignment.Center, FontSize = buttonSize, Margin = new Thickness(10, 0, 10, 10) };

            Children.Add(text,()=> new Rectangle(0,0,Width, Height - buttonSize * 3));

            var button = new Button() { Text = "Понятно", FontSize = buttonSize * 1.2f, Margin = new Thickness(10, 0, 10, 10), BackgroundColor = Color.Green };
            Children.Add(button, () => new Rectangle(0, Height-buttonSize*3, Width, buttonSize*3));

            button.Clicked += Click;
        }



        public void Click(object sender, EventArgs e)
        {
         var s= text.GetSizeRequest(Width,Height);

            Debug.WriteLine(s.Request);

            Debug.WriteLine(s.Minimum.Height/text.FontSize);


            Debug.WriteLine(new Xamarin.Forms.Size(Width, Height));


            PlayablePlayerController.Get.Next();
        }
    }
}
