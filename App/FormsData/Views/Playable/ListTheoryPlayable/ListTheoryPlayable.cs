﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using App.Data.Playables.Theory;
using App.FormsData.Controllers;
using App.FormsData.Views.Content;
using Xamarin.Forms;

namespace App.FormsData.Views.Playable
{
    public class ListTheoryPlayableView : BasePlayableView
    {
   double buttonSize = Device.GetNamedSize(NamedSize.Large,typeof(Button));

        Xamarin.Forms.Label text;


        public ListTheoryPlayableView(ListTheoryPlayable playable) : base(playable)
        {
            var stack = new StackLayout();



            text = new Text(playable.Text) { VerticalTextAlignment = TextAlignment.Start, HorizontalTextAlignment = TextAlignment.Center, FontSize = buttonSize, Margin = new Thickness(10, 0, 10, 10) };

            var scroll = new ScrollView() { Orientation = ScrollOrientation.Vertical };


            stack.Children.Add(text);
            stack.Children.Add(scroll);

            Children.Add(stack, () => new Rectangle(0, 0, Width, Height - buttonSize * 3));

            var button = new Button() { Text = "Понятно", FontSize = buttonSize * 1.2f, Margin = new Thickness(10, 0, 10, 10), BackgroundColor = Color.Green };
            Children.Add(button, () => new Rectangle(0, Height - buttonSize * 3, Width, buttonSize * 3));





            var stackContent = new StackLayout();
            scroll.Content = stackContent;


            Debug.WriteLine(123123123123);
            foreach (var item in playable.List)
            {
                stackContent.Children.Add(new TextListTheory(item) { VerticalTextAlignment = TextAlignment.Start, HorizontalTextAlignment = TextAlignment.Center, FontSize = buttonSize*2, Margin = new Thickness(10, 0, 10, 10) });
            }





        

         



   

            button.Clicked += Click;
        }



        public void Click(object sender, EventArgs e)
        {
         var s= text.GetSizeRequest(Width,Height);

            Debug.WriteLine(s.Request);

            Debug.WriteLine(s.Minimum.Height/text.FontSize);


            Debug.WriteLine(new Xamarin.Forms.Size(Width, Height));


            PlayablePlayerController.Get.Next();
        }
    }
}
