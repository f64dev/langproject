﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using App.Data.Playables;
using App.Data.Playables.Theory;
using App.FormsData.Controllers;
using App.FormsData.Views.Content;
using App.FormsData.Views.Grammar.UnitRender.Types;
using Xamarin.Forms;

namespace App.FormsData.Views.Playable
{
    public class TextListTheory : Content.Text
    {
        public TextListTheory(string content) : base(content) { }
      

        public override double GetSize => Device.GetNamedSize(NamedSize.Large, typeof(Xamarin.Forms.Label)) * 1.3;
    }
}
