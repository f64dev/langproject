﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using App.FormsData.Views.ExercisesType.Base;
using Xamarin.Forms;

namespace App.FormsData.Views.ExercisesType
{
	public class ChoiceTwoText : BaseLayout
	{
        #region Views

        Button button1, button2;

        #endregion
        #region Init

        public ChoiceTwoText() 
        {
            var bar = CreateDefaultBarLayout("Слава украине");
   

            var button1 = new Button()
            {
                Text = "Верно",
                BackgroundColor = Color.IndianRed
            };
            var button2 = new Button()
            {
                Text = "Неверно",
                BackgroundColor = Color.LightSeaGreen
            };

            Label l;
          
            SetDynamicFont(button1, .3f);

            SetDynamicFont(button2, .3f);


            var buttons = CreateChoiceLayout(10,button1,button2);

            Init(.07f,.3f,bar, new RelativeLayout(), buttons);
        }

        #endregion
    }
}