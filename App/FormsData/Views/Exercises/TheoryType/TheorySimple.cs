﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using App.FormsData.Views.ExercisesType.Base;
using Lottie.Forms;
using Xamarin.Forms;

namespace App.FormsData.Views.TheoryType
{
	public class TheorySimple : BaseLayout
	{
        #region Views

        Button button1;

        #endregion
        #region Init

        public TheorySimple()
        {
            var bar = CreateDefaultBarLayout("Что такое глагол?");


            button1 = new Button()
            {
                Text = "Понятно",
                BackgroundColor = Color.Green
            };

            SetDynamicFont(button1, .3f);


            var buttons = CreateChoiceLayout(10, button1);

            var a = new AnimationView()
            {
               Animation= "Animations/test.json",
               AutoPlay=true
            };


            Init(.07f, .1f, bar, a, buttons);
        }

        #endregion
    }
}