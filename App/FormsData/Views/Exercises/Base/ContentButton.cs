﻿using Xamarin.Forms;

namespace App.FormsData.Views.ExercisesType
{
        public class ContentButton
        {
            public RelativeLayout Layout { get; private set; }

            public Button Button { get; private set; }
            public View Content { get; private set; }



            public ContentButton(View content)
            {
                Layout = new RelativeLayout();
                Button = new Button();
                Button.Opacity = 0;
                Content = content;

                Layout.Children.Add
                (
                    view: Content,
                    xConstraint: Constraint.Constant(0),
                    yConstraint: Constraint.Constant(0),
                    widthConstraint: Constraint.RelativeToParent((p) => p.Width),
                    heightConstraint: Constraint.RelativeToParent((p) => p.Height)
                );

                Layout.Children.Add
               (
                   view: Button,
                   xConstraint: Constraint.Constant(0),
                   yConstraint: Constraint.Constant(0),
                   widthConstraint: Constraint.RelativeToParent((p) => p.Width),
                   heightConstraint: Constraint.RelativeToParent((p) => p.Height)
               );
            }


            public static implicit operator View(ContentButton button)=> button.Layout;
        }

}