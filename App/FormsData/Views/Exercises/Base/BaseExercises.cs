﻿using System;
using Xamarin.Forms;

namespace App.FormsData.Views.ExercisesType
{
    public abstract partial class BaseExercisesView : ContentView
    {
        #region Init

        public RelativeLayout MainLayout { get; protected set; }
  

        public BaseExercisesView()
        {
            #region MainLayout

            MainLayout = new RelativeLayout();
            Content = MainLayout;

            #endregion
        }

        #endregion
        #region Font

        public void SetDynamicFont(Button view,float factor)
        {
            view.SizeChanged += (object sender, EventArgs e) =>{ view.FontSize = (view.Height+view.Margin.Top + view.Margin.Bottom) * factor; };
        }


        public void SetDynamicFont(Label view, float factor)
        {
            view.SizeChanged += (object sender, EventArgs e) => { view.FontSize = (view.Height + view.Margin.Top + view.Margin.Bottom) * factor; };
        }

#endregion
    }
}