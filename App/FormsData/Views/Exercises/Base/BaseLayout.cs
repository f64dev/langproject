﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Lottie.Forms;
using Xamarin.Forms;

namespace App.FormsData.Views.ExercisesType.Base
{
    public class BaseLayout : BaseExercisesView
    {
        #region Colors

        public Color barColor = Color.FromHex("#f4f8ff");
        public Color contentColor = Color.White;
        public Color choiceColor = Color.FromHex("#f4f8ff");
        public Color lineColor = Color.SandyBrown;

        #endregion
        #region Layouts

        public RelativeLayout BarLayout { get; protected set; }
        public View ContentLayout { get; protected set; }
        public RelativeLayout ChoiceLayout { get; protected set; }

        #endregion
        #region Init

        protected void Init(float barPercent , float choicePercent, RelativeLayout barLayout, View contentLayout, RelativeLayout choiceLayout)
        {
            #region BarLayout

            BarLayout = barLayout;
            BarLayout.BackgroundColor = barColor;

            MainLayout.Children.Add
            (
                view: BarLayout,
                heightConstraint: Constraint.RelativeToParent((p) => p.Height * barPercent),
                widthConstraint: Constraint.RelativeToParent((p) => p.Width)
            );

            #endregion
            #region Content

            ContentLayout = contentLayout;
            ContentLayout.BackgroundColor = contentColor;

            MainLayout.Children.Add
            (
                view: ContentLayout,
                heightConstraint: Constraint.RelativeToParent((p) => p.Height * (1-(barPercent+choicePercent))),
                widthConstraint: Constraint.RelativeToParent((p) => p.Width),
                xConstraint: Constraint.Constant(0),
                yConstraint: Constraint.RelativeToParent((p) => p.Height * barPercent)
            );

            #endregion
            #region Choice

            ChoiceLayout = choiceLayout;
            ChoiceLayout.BackgroundColor = choiceColor;

            MainLayout.Children.Add
            (
                view: ChoiceLayout,
                heightConstraint: Constraint.RelativeToParent((p) => p.Height * choicePercent),
                widthConstraint: Constraint.RelativeToParent((p) => p.Width),
                xConstraint: Constraint.Constant(0),
                yConstraint: Constraint.RelativeToParent((p) => p.Height - p.Height * choicePercent)
            );

            #endregion
            #region Line

            var downLine = new BoxView() { BackgroundColor = lineColor };

            ChoiceLayout.Children.Add
            (
                view: downLine,
                heightConstraint: Constraint.Constant(1),
                widthConstraint: Constraint.RelativeToParent((p) => p.Width),
                xConstraint: Constraint.Constant(0),
                yConstraint: Constraint.Constant(0)
            );


            var upLine = new BoxView() { BackgroundColor = lineColor };

            BarLayout.Children.Add
            (
                view: upLine,
                heightConstraint: Constraint.Constant(1),
                widthConstraint: Constraint.RelativeToParent((p) => p.Width),
                xConstraint: Constraint.Constant(0),
                yConstraint: Constraint.RelativeToParent((p) => p.Height)
            );

            #endregion
        }

        #endregion
        #region Bar

        public RelativeLayout CreateDefaultBarLayout(string text)
        {
            var layout = new RelativeLayout();

            var a = new AnimationView()
            {
                Animation = "Animations/test2.json", AutoPlay=true
            };
           // a.OnPlay+=(s,d)=> { a.Pause();  };

          //  a.OnClick += (s,d)=> { Debug.WriteLine("test"); } ;

            



       


            var buttonMenuImage = new Image();
            buttonMenuImage.Source = "Icons/Menu.png";
            var buttob = new ContentButton(buttonMenuImage);
            buttob.Button.Clicked += (s, d) =>
            {
                var r = new RelativeLayout();
                r.Opacity = 0.4f;
                r.BackgroundColor = Color.Accent;

                MainLayout.Children.Add(r,
                heightConstraint: Constraint.RelativeToParent((p) => p.Height),
                widthConstraint: Constraint.RelativeToParent((p) => p.Width ),
                xConstraint: Constraint.RelativeToParent((p) =>0),
                yConstraint: Constraint.RelativeToParent((p) => 0));
            };

            //  buttonMenuImage.Source = ImageSource.FromStream(() =>
            // AppResources.GetEmbeddedResourceStream("Icons.Menu.png"));

            var textBar = new Label();
            textBar.Text = text;
            SetDynamicFont(textBar, .5f);

            textBar.VerticalTextAlignment = TextAlignment.Center;
            textBar.HorizontalTextAlignment = TextAlignment.Center;


            layout.Children.Add
            (
                view: buttob.Layout,
                heightConstraint: Constraint.RelativeToParent((p) => p.Height),
                widthConstraint: Constraint.RelativeToParent((p) => p.Height)
            );




            layout.Children.Add
            (
                view: textBar,
                heightConstraint: Constraint.RelativeToParent((p) => p.Height),
                widthConstraint: Constraint.RelativeToParent((p) => p.Width - p.Height),
                xConstraint: Constraint.RelativeToParent((p) => p.Height),
                yConstraint: Constraint.RelativeToParent((p) => 0)
            );

            return layout;
        }

        private void A_OnClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }



        #endregion
        #region Choice

        public RelativeLayout CreateChoiceLayout(float margin, params View[] views)
        {
            var count = views.Length;
            var layout = new RelativeLayout();

            #region Layout

            foreach (int i in Enumerable.Range(0, count))
            {
                var view = views[i];

                layout.Children.Add
                (
                    view: view,
                    heightConstraint: Constraint.RelativeToParent((p) => p.Height / count),
                    widthConstraint: Constraint.RelativeToParent((p) => p.Width),
                    xConstraint: Constraint.Constant(0),
                    yConstraint: Constraint.RelativeToParent((p) => (p.Height / count) * i)
                );

                view.Margin = new Thickness(margin, margin, margin, (i == count - 1) ? margin : 0);
            }

            #endregion

            return layout;
        }

        #endregion
    }
}
