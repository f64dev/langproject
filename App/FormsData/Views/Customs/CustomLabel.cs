﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace App.FormsData.Views.Customs
{
	public class CustomLabel : RelativeLayout
	{
		public CustomLabel ()
		{
            var label = new Label();

            var span1 = new Span() { Text = "Как написать текст на страницу " };
            var span2 = new Span() { Text = "«О компании» и выбросить из словосочетания «потенциальный клиент»", ForegroundColor = Color.Blue, FontAttributes = FontAttributes.Bold };

            var s = new FormattedString();
            s.Spans.Add(span1);
            s.Spans.Add(span2);

        

            var tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += (sender, e) =>
            {
                // cast to an image
                Image theImage = (Image)sender;

                // now you have a reference to the image
            };

            label.FormattedText = s;

         

          

            var l = new StackLayout();
            l.Orientation = StackOrientation.Horizontal;


            var text1 = new Label() { Text = "Как написать текст на страницу " , HorizontalTextAlignment=TextAlignment.End};
            var text2 = new Label() { Text = "«О компании» и выбросить из словосочетания «потенциальный клиент»", HorizontalTextAlignment = TextAlignment.Center };
            var text3 = new Label() { Text = "«О компании» и выбросить из словосочетания «потенциальный клиент»", HorizontalTextAlignment = TextAlignment.Start };

            l.Children.Add(text1);
            l.Children.Add(text2);
            l.Children.Add(text3);





            var box = new BoxView();


            Children.Add(l,
             xConstraint: Constraint.Constant(0),
             yConstraint: Constraint.Constant(0),
             widthConstraint: Constraint.RelativeToParent(p => p.Width),
             heightConstraint: Constraint.RelativeToParent(p => p.Height));



            box.BackgroundColor= Color.Red;
            Children.Add(box,
            xConstraint: Constraint.Constant(0),
            yConstraint: Constraint.RelativeToParent(p => 
            {
                return p.Height / 2;
            }),
            widthConstraint: Constraint.RelativeToParent(p => 50),
            heightConstraint: Constraint.RelativeToParent(p => label.FontSize));
        }

        
    }
}