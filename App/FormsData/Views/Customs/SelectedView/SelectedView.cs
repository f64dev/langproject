﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace App.FormsData.Views.Customs
{
    public class SelectedView<TSelector, TButton> : RelativeLayout where TSelector : View where TButton : View
    {
        public TButton[] Buttons { get; private set; }
        public TSelector Selector { get; private set; }

        public Action<int> SelectedChange { get; set; }


        public Animation ChangeAnimation { get; set; }
        public uint AnimationLength { get; set; } = 500;
        public Easing AnimationEasing { get; set; } = Easing.CubicInOut;

        private int selected;
        public int Selected
        {
            get => selected;
            set
            {
                if (selected == value)
                    return;

                PrevSelected = selected;
                selected = value;


                if (ChangeAnimation == null)
                    Selector.TranslationX = Width / Buttons.Length * selected;
                else
                    ChangeAnimation.Commit(this, "Change", length: AnimationLength, easing: AnimationEasing);
            }
        }

        public int PrevSelected { get; private set; }


        public SelectedView(TSelector selector, TButton[] buttons = null)
        {
            Selector = selector;

            if (Buttons != null)
                InitButtons(buttons);
        }


        public void InitButtons(TButton[] buttons)
        {

            Buttons = buttons;

            foreach (var idx in Enumerable.Range(0, Buttons.Length))
            {
                var button = buttons[idx];

                var commamd = new Command(() =>
                {
                    if (Selected == idx)
                        return;

                    Selected = idx;
                    SelectedChange?.Invoke(idx);
                });

                button.GestureRecognizers.Add(new TapGestureRecognizer() { Command = commamd });



                Children.Add(Selector, () => new Rectangle(0, 0, Width / Buttons.Length, Height));

                Children.Add(button, () => new Rectangle(Width / Buttons.Length * idx, 0, Width / Buttons.Length, Height));
            }
        }
    }
}