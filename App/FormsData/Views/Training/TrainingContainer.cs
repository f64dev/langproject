﻿using System.Diagnostics;
using App.FormsData.Behaviors;
using App.FormsData.Behaviors.Main;
using App.FormsData.Controllers;
using App.FormsData.Views.Customs;
using App.FormsData.Views.Main.Content;
using App.FormsData.Views.Training.Bar;
using App.Framework;
using Training;
using Xamarin.Forms;

namespace App.FormsData.Views.Content.Training
{
    public class TrainingContainer :RelativeLayout,IMainContent,IStackContent
	{
        #region Content

        public string Title => "Обучение";

        public void OnAdd()
        {
        }

        public void OnRemove()
        {
            Behaviors.Clear();

            TrainingController.Get.CurrentTimelinePosition = TimelineContainer.ScrollY;

 

                TrainingController.Release();

        }

        public void OnCurrent()
        {
          Behaviors.Add(new OpenMenu());
        }

        public void OnPrevious()
        {
            Behaviors.Clear();

        }

        #endregion
        #region Init

        public NavigationBarView TrackSelector { get; private set; }

        public TimelineContainer TimelineContainer { get; private set; }


        public TrainingContainer()
        {
            TrainingController.Init();


       


            BackgroundColor = Color.Red;

            #region Timeline

          //  TimelineContainer = new TimelineContainer((global::App.Data.Training.Timeline)global::App.Data.Training.TrainingUtility.GetLevels().root[(int)0].GetTimeline());

            Children.Add
            (
                view: TimelineContainer,
                heightConstraint: Constraint.RelativeToParent((p) => p.Height - Device.GetNamedSize(NamedSize.Medium, typeof(Label)) * 2f),
                widthConstraint: Constraint.RelativeToParent((p) => p.Width),
                xConstraint: Constraint.Constant(0),
                yConstraint: Constraint.Constant(0)
            );

            #endregion
            #region NavigationBar

          //  TrackSelector = new NavigationBarView((global::App.Data.Training.Timeline)global::App.Data.Training.TrainingUtility.GetLevels().root[(int)0].GetTimeline());

          

            Children.Add
            (
                view: TrackSelector,
                heightConstraint: Constraint.RelativeToParent((p) => Device.GetNamedSize(NamedSize.Medium,typeof(Label))*2f),
                widthConstraint: Constraint.RelativeToParent((p) => p.Width),
                xConstraint: Constraint.Constant(0),
                yConstraint: Constraint.RelativeToParent((p) => p.Height  - Device.GetNamedSize(NamedSize.Medium, typeof(Label)) * 2f)
            );

            #endregion


             Device.StartTimer(new System.TimeSpan(0,0,1), () => { TimelineContainer.ScrollToAsync(0, TrainingController.Get.CurrentTimelinePosition, false); return false; });


        
        }

        #endregion
    }
}