﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using App.FormsData.Views.Training.Bar;
using App.FormsData.Controllers;
using App.FormsData.Views.Customs;
using Xamarin.Forms;

namespace App.FormsData.Views.Training.Bar
{
	public class NavigationBarView : SelectedView<SelectorView, ButtonLabel>
    {
        Color backgroundColor =Color.PaleVioletRed;






		public NavigationBarView(global::App.Data.Modules.Learning.Timeline timeline):base(new SelectorView())
		{

            BackgroundColor= backgroundColor;

            var tracks = timeline.root;

            var count = timeline.root.Length;

            var buttons = new ButtonLabel[count];

            for (int i = 0; i < count; i++)
                buttons[i] = new ButtonLabel() { Text=tracks[i].folder,Type= tracks[i].type};

            SelectedChange = (idx) =>
            {
             //   TrainingController.Get.TimelineStack.Push(Buttons[idx].Type, PrevSelected < Selected);
            };



            InitButtons(buttons);
        }

       
    }
}