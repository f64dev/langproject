﻿using System;
using System.Collections.Generic;
using System.Text;
using App.Data.Modules.Learning;
using Xamarin.Forms;

namespace App.FormsData.Views.Training.Bar
{
    public class ButtonLabel : Label
    {
     


        public int Idx { get; set; }

        public TimelineTrackType Type { get; set; }


        public ButtonLabel()
        {
            VerticalTextAlignment = TextAlignment.Center;
            HorizontalTextAlignment = TextAlignment.Center;
 


        }


        public ButtonLabel(TimelineTrack track, int idx, Command command)
        {
            Text = track.folder;
            VerticalTextAlignment = TextAlignment.Center;
            HorizontalTextAlignment = TextAlignment.Center;
            Idx = idx;
            Type = track.type;


            var tapGestureRecognizer = new TapGestureRecognizer() { Command = command };


            GestureRecognizers.Add(tapGestureRecognizer);

        }
    }
}
