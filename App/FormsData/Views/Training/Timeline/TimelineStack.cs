﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using App.Data.Modules.Learning;
using App.FormsData.Controllers;
using App.FormsData.Views.Customs;
using App.FormsData.Views.Training.Timeline.Tracks;
using App.Framework;
using Xamarin.Forms;

namespace App.FormsData.Views.Content.Training.Timeline
{
	public class TimelineStack : RelativeStackContainerLayout
    {
        private App.Data.Modules.Learning. Timeline timeline;
        private int count;

        public TimelineStack (App.Data.Modules.Learning.Timeline timeline, int count):base()
		{
            this.timeline = timeline;
            this.count = count;

            var v = TrainingController.GetTrackView(timeline.GetTrack(TimelineTrackType.Theory), count);

            Add(v);

            TrainingController.Get.TimelineStack = this;


            rightAnimation = new Animation((t) =>
            {
                CurrentElement.TranslationX = Width * (t-1);

                PreviousElement.TranslationX = Width * t;



            }, easing: Easing.CubicInOut);


           leftAnimation = new Animation((t) =>
            {
                CurrentElement.TranslationX = Width *(1- t);
                PreviousElement.TranslationX = Width * -t;

  
            }, easing: Easing.CubicInOut);

        }




        public Animation leftAnimation;

        public Animation rightAnimation;


        public void Push(TimelineTrackType timelineTrackType,bool isLeft,Action start=null, Action end= null)
        {
            TrainingController.Get.CurrentTimelineTrackType = timelineTrackType;


        var v = TrainingController.GetTrackView(timeline.GetTrack(timelineTrackType), count);

            PushAnimation = isLeft ? leftAnimation : rightAnimation;

            base.Set(v,500);
        }

	}
}