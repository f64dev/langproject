﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using App.FormsData.Controllers;
using Xamarin.Forms;

namespace App.FormsData.Views.Content.Training.Timeline
{
	public class TimelineBackground : RelativeLayout
	{
        private float lineLenght=2f;


		public TimelineBackground (int count)
		{
            foreach (var iter in Enumerable.Range(0, count))
            {
                var line = new BoxView()
                {
                    BackgroundColor = Color.Chartreuse
                };

                var label = new Label()
                {
                    FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label)),
                    Text = $"{(int)(100 - (iter / (count * 0.01f)))}%",
                };


                Children.Add(line,
                xConstraint: Constraint.Constant(0),
                yConstraint: Constraint.RelativeToParent((p) => p.Height / count * iter),
                widthConstraint: Constraint.RelativeToParent((p) => p.Width),
                heightConstraint: Constraint.Constant(lineLenght));


                Children.Add(label,
                    xConstraint: Constraint.Constant(2),
                    yConstraint: Constraint.RelativeToParent((p) => p.Height/count*iter+lineLenght));

            }
            TrainingController.Get.TimelineBackground = this;
        }
    }
}