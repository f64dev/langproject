﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using App.Data.Modules.Learning;
using App.FormsData.Views.Training.Timeline.Elements;
using Xamarin.Forms;

namespace App.FormsData.Views.Training.Timeline.Tracks
{
    public class TheoryTrack : BaseTrack<TheoryElement>
    {
        public TheoryTrack(TimelineTrack track, int count) : base(track, count)
        {
            color = Color.LightPink;
        }
    }
}