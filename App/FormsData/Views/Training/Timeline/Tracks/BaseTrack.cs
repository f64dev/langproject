﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using App.Data.Modules.Learning;
using App.FormsData.Views.Customs;
using App.FormsData.Views.Training.Timeline.Elements;
using App.Framework;
using Xamarin.Forms;

namespace App.FormsData.Views.Training.Timeline.Tracks
{
    public abstract class BaseTrack<Element> : RelativeLayout, IStackContent where Element : BaseElement, new()
    {
        public Color color;


        public Element[] Elements {get;protected set;}


        public BaseTrack(TimelineTrack track, int count)
        {
            var length = track.elements.Length;

            Elements = new Element[length];

            for (int i = 0; i < length; i++)
            {
                var e = track.elements[i];

                var pos = count - e.pos;

                var element = new Element();

                element.Init(e);

                Children.Add(element,
                    xConstraint: Constraint.Constant(0),
                    widthConstraint: Constraint.RelativeToParent((p) => p.Width),
                    yConstraint: Constraint.RelativeToParent((p) => p.Height / count * pos),
                    heightConstraint: Constraint.RelativeToParent((p) => p.Height / count));

                Elements[i] = element;
            }
        }


        public void OnAdd()
        {
            
        }

        public void OnRemove()
        {
            foreach (var e in Elements)
                e.OnDestroy();
        }
        public void OnCurrent()
        {

        }

        public void OnPrevious()
        {

        }

    }
}