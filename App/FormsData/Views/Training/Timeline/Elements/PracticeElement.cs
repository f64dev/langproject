﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using App.Data.Modules.Learning;
using Xamarin.Forms;

namespace App.FormsData.Views.Training.Timeline.Elements
{
	public class PracticeElement : BaseElement
	{
        public PracticeElement()
        {

            BackgroundColor = Color.Beige;


        }

        public override void Init(TimelineElement element)
        {
            base.Init(element);

            Text = element.name;
            
        }
    }
}