﻿using System;
using System.Collections.Generic;
using System.Text;
using App.Data.Modules.Learning;
using Xamarin.Forms;

namespace App.FormsData.Views.Training.Timeline.Elements
{
   public class TheoryElement :BaseElement
    {
        public TheoryElement()
        {

            BackgroundColor = Color.DarkRed;


        }

        public override void Init(TimelineElement element)
        {
            base.Init(element);
   
            Text = element.name;
        }


    }
}
