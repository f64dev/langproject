﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using App.Data.Modules.Learning;
using App.FormsData.Behaviors.Training;
using Xamarin.Forms;

namespace App.FormsData.Views.Training.Timeline.Elements
{
	public abstract class BaseElement : Button
	{



		public BaseElement ()
		{
            Margin = new Thickness(20);

        }
        public TimelineElement element;

        public virtual void Init(TimelineElement element)
        {
            this.element = element;

            OnCreate();

        }

        public void OnCreate()
        {
            Behaviors.Add(new ElementAction());
        }

        public void OnDestroy()
        {
            Behaviors.Clear();
        }
	}
}