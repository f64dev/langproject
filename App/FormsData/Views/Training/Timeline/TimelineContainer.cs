﻿using App.Data.Modules.Learning;
using App.FormsData.Views.Content.Training.Timeline;
using Xamarin.Forms;

namespace Training
{
    public class TimelineContainer : ScrollView
    {
        #region Init

        public TimelineContainer(Timeline timeline)
        {
            #region Content

            Orientation = ScrollOrientation.Vertical;

            var relativeLayout = new RelativeLayout();
            Content = relativeLayout;

            var count = timeline.GetTimelineLength();

            relativeLayout.HeightRequest = sizeElement * count;

            #endregion
            #region Background

            var background = new TimelineBackground(count);

            relativeLayout.Children.Add
            (
                view: background,
                widthConstraint: Constraint.RelativeToParent((p) => p.Width),
                heightConstraint: Constraint.RelativeToParent((p) => p.Height),
                xConstraint: Constraint.Constant(0),
                yConstraint: Constraint.Constant(0)
            );

            #endregion
            #region Timeline

            var stack = new TimelineStack(timeline,count);

            relativeLayout.Children.Add
            (
                view: stack,
                widthConstraint: Constraint.RelativeToParent((p) => p.Width),
                heightConstraint: Constraint.RelativeToParent((p) => p.Height),
                xConstraint: Constraint.Constant(0),
                yConstraint: Constraint.Constant(0)
            );

            #endregion
        }

        #endregion
        #region Size

        private float sizeElement = 160;

        private double GetHeightView(Timeline timeline)
        {
            var position = 0;

            foreach (var e in timeline.GetTrack(TimelineTrackType.Theory).elements)
                if (e.pos > position)
                    position = e.pos;

            return sizeElement * position;
        }

        #endregion
    }
}
