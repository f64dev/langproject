﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using App.FormsData.Controllers;
using App.FormsData.Views.Content;
using App.FormsData.Views.Main;
using App.Framework;
using Xamarin.Forms;

namespace App.FormsData.Behaviors
{
    public class ClickButton : Behavior<View>
    {
        public ClickButton(Button button, EventHandler acton)
        {
            this.button = button;
            this.acton = acton;
        }

        private Button button;
        private EventHandler acton;


        protected override void OnAttachedTo(View view)
        {
            button.Clicked += acton;

            base.OnAttachedTo(view);
        }

        protected override void OnDetachingFrom(View entry)
        {
            button.Clicked -= acton;

            button = null;
            acton = null;

            base.OnDetachingFrom(entry);
        }
    }
}
