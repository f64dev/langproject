﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using App.FormsData.Controllers;
using App.Framework;
using App.FormsData.Views.Content;
using Xamarin.Forms;

namespace App.FormsData.Behaviors.Main
{
    public class OpenMenu : Behavior<View>
    {
        private Animation openMenuAnimation;

        protected override void OnAttachedTo(View entry)
        {
            var content = AppFramework.Content;
          // var   bar = PageController.Page.GetBar<Views.Bars.MainBar>();

            openMenuAnimation = new Animation((t) =>
            {
                content.CurrentElement.TranslationY = content.Height * (t - 1);

            //    bar.Title.Opacity = 1 - t;

            },easing:Easing.CubicInOut);

          //  bar.Menu.OnClick+= Click;

            base.OnAttachedTo(entry);
        }

        protected override void OnDetachingFrom(View entry)
        {
        //    var bar = PageController.Page.GetBar<Views.Bars.MainBar>();

          //  bar.Menu.OnClick -= Click;

            base.OnDetachingFrom(entry);
        }


        public void Click(object sender, EventArgs e)
        {
            var content = AppFramework.Content;
            var menu = ModulesController.Get.CreateMenu();

         //   var bar = PageController.Page.GetBar<Views.Bars.MainBar>();

            content.PushAnimation = openMenuAnimation;


            Action start = () =>
            {
             //   var page = PageController.Page;

             //   page.EnableInput = false;

            //    bar.Menu.PlayOpenMenuButtonAnim();
            };

            Action end = () =>
            {
               // var page = PageController.Page;

             //   page.EnableInput = true;
            };


            content.Push(menu, 500, start: start, end: end);
        }
    }
}
