﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using App.FormsData.Controllers;
using App.FormsData.Views.Content;
using App.FormsData.Views.Main;
using App.Framework;
using Xamarin.Forms;

namespace App.FormsData.Behaviors.Main
{
    public class CloseMenu : Behavior<MenuContent>
    {
        private Animation openMenuAnimation;


        protected override void OnAttachedTo(MenuContent entry)
        {
            var content = AppFramework.Content;
          //  var bar = PageController.Page.GetBar<Views.Bars.MainBar>();

            openMenuAnimation = new Animation((t) =>
            {
                content.CurrentElement.TranslationY = content.Height * -t;

             //   bar.Title.Opacity = t;

            }, easing: Easing.CubicInOut);

          //  bar.Menu.OnClick += Click;

            base.OnAttachedTo(entry);
        }

        protected override void OnDetachingFrom(MenuContent entry)
        {
          //  var bar = PageController.Page.GetBar<Views.Bars.MainBar>();

          //  bar.Menu.OnClick -= Click;

       

            base.OnDetachingFrom(entry);
        }


        public void Click(object sender, EventArgs e)
        {
            var content = AppFramework.Content;
           // var bar = PageController.Page.GetBar<Views.Bars.MainBar>();

            content.PopAnimation = openMenuAnimation;


            Action start = () =>
            {
                AppFramework.EnableInput = false;

              //  bar.Menu.PlayCloseMenuButtonAnim();
            };

            Action end = () =>
            {
                AppFramework.EnableInput = true;
            };


            content.Pop(500, start: start, end: end);
        }
    }
}
