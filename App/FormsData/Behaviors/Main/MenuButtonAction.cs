﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using App.Data.Modules;
using App.FormsData.Controllers;
using App.FormsData.Views.Main;
using App.Framework;
using Xamarin.Forms;

namespace App.FormsData.Behaviors.Main
{
	public class MenuButtonAction : Behavior<MenuButton>
	{
        private Animation animation;
        private ModuleType type;


        protected override void OnAttachedTo(MenuButton button)
        {
            var content = AppFramework.Content;
        //   var bar = PageController.Page.GetBar<Views.Bars.MainBar>();
           type = button.type;

            animation = new Animation((t) =>
            {
                content.CurrentElement.TranslationX = content.Width * (1-t);

              //  bar.Title.Text = button.Text;

            //  bar.Title.Opacity = t;

            }, easing: Easing.CubicInOut);

            button.Clicked += Click;


            base.OnAttachedTo(button);
        }

        protected override void OnDetachingFrom(MenuButton button)
        {

            button.Clicked -= Click;



            base.OnDetachingFrom(button);
        }





        public void Click(object sender, EventArgs e)
        {
            var content = AppFramework.Content;


          //  ModulesController.Get.CurrentContentType = type;

            var c = ModulesController.Get.CreateCurrentContentContainer();
        //    var bar = PageController.Page.GetBar<Views.Bars.MainBar>();


            content.PushAnimation = animation;


            Action start = () =>
            {
            //    var page = PageController.Page;

             //   page.EnableInput = false;

            //    bar.Menu.PlayCloseMenuButtonAnim();
            };

            Action end = () =>
            {
               // var page = PageController.Page;

              //  page.EnableInput = true;

            };


          content.Set(c, 500, start: start, end: end);
        }









    }
}