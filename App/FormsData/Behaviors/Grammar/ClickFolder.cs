﻿using System;
using System.Collections.Generic;
using System.Text;
using App.Data.Modules.Directory;
using App.FormsData.Controllers;
using App.FormsData.Controllers.Content;
using App.FormsData.Views.Content.Grammar;
using App.FormsData.Views.Content.Grammar.Structure;
using Xamarin.Forms;

namespace App.FormsData.Behaviors.Grammar
{
    public class ClickFolder : Behavior<FolderElement>
    {
        private Animation openMenuAnimation;
        protected StructureElement element;

        protected override void OnAttachedTo(FolderElement entry)
        {
            var content = GrammarController.Get.GrammarContent;
           // var bar = PageController.Page.GetBar<Views.Bars.MainBar>();

            element = entry.element;


            openMenuAnimation = new Animation((t) =>
            {
                content.CurrentElement.TranslationX = content.Width * (1 - t);

              //  bar.Title.Opacity = t;
            }, easing: Easing.CubicInOut);

            openMenuAnimation.Add(0, 0.5, new Animation((t) =>
            {
             //   bar.Title.Opacity = 1 - t;
            },
            finished: () =>
             {
                 var list = content.CurrentElement as UnitList;

               //  bar.Title.Text = list.Title;
             }));

            openMenuAnimation.Add(0.5, 1, new Animation((t) =>
            {
               // bar.Title.Opacity = t;
            }));

            entry.Clicked += Click;

            base.OnAttachedTo(entry);
        }

        protected override void OnDetachingFrom(FolderElement entry)
        {
            // var bar = PageController.Page.GetBar<Views.Bars.MainBar>();

            entry.Clicked -= Click;

            element = null;

            base.OnDetachingFrom(entry);
        }


        public void Click(object sender, EventArgs e)
        {
            var content = GrammarController.Get.GrammarContent;
          //  var bar = PageController.Page.GetBar<Views.Bars.MainBar>();
            var el = element;

            content.PushAnimation = openMenuAnimation;


            Action start = () =>
            {
              //  var page = PageController.Page;

             //   page.EnableInput = false;

              //  if (content.Stack.Count == 2)
                //    bar.Menu.PlayMenuToBack();
            };

            Action end = () =>
            {
               // var page = PageController.Page;

              //  page.EnableInput = true;
            };


          //  content.Push(new UnitList(el), 500, start: start, end: end);
        }
    }
}
