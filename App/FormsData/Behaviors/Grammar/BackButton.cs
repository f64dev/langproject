﻿using System;
using System.Collections.Generic;
using System.Text;
using App.FormsData.Controllers;
using App.FormsData.Controllers.Content;
using App.FormsData.Views.Content.Grammar;
using App.FormsData.Views.Content.Grammar.Structure;
using App.Framework;
using Xamarin.Forms;

namespace App.FormsData.Behaviors.Grammar
{

    public class BackButton : Behavior<View>
    {
        private Animation openMenuAnimation;


        protected override void OnAttachedTo(View entry)
        {
            var content = GrammarController.Get.GrammarContent;
          //  var bar = PageController.Page.GetBar<Views.Bars.MainBar>();


            openMenuAnimation = new Animation((t) =>
            {
                content.CurrentElement.TranslationX = content.Width * -t;

              //  bar.Title.Opacity = t;
            }, easing: Easing.CubicInOut);

            openMenuAnimation.Add(0, 0.5, new Animation((t) =>
            {
               // bar.Title.Opacity = 1 - t;
            },
            finished: () =>
            {
                 var list = content.PreviousElement as UnitList;

              //   bar.Title.Text = list?.Title;
            }));

            openMenuAnimation.Add(0.5, 1, new Animation((t) =>
            {
              //  bar.Title.Opacity = t;
            }));

           // bar.Menu.OnClick += Click;

            base.OnAttachedTo(entry);
        }

        protected override void OnDetachingFrom(View entry)
        {
          //  var bar = PageController.Page.GetBar<Views.Bars.MainBar>();

          //  bar.Menu.OnClick -= Click;

            base.OnDetachingFrom(entry);
        }


        public void Click(object sender, EventArgs e)
        {
            var content = GrammarController.Get.GrammarContent;
            var bar = AppFramework.Bar;

            content.PopAnimation = openMenuAnimation;


            Action start = () =>
            {
                AppFramework.EnableInput = false;

              //  if (content.Stack.Count == 1)
                //    bar.Menu.PlayBackToMenu();
            };

            Action end = () =>
            {
                AppFramework.EnableInput = true;
            };


            content.Pop(500, start: start, end: end);
        }
    }
}
