﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using App.Data.Modules.Learning;
using App.FormsData.Controllers;
using App.FormsData.Views.Main;
using App.FormsData.Views.Training.Timeline.Elements;
using Xamarin.Forms;

namespace App.FormsData.Behaviors.Training
{
	public class ElementAction : Behavior<BaseElement>
	{
        private TimelineElement element;

        protected override void OnAttachedTo(BaseElement button)
        {
            element = button.element;


            button.Clicked += Click;


            base.OnAttachedTo(button);
        }


        
        protected override void OnDetachingFrom(BaseElement button)
        {
            button.Clicked -= Click;

            base.OnDetachingFrom(button);
        }



        public void Click(object sender, EventArgs e)
        {
            PlayablePlayerController.Init();
         //   PlayablePlayerController.Get.Start(element.GetExercise(),0);
        }
    }
}