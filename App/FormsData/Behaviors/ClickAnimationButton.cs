﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using App.FormsData.Controllers;
using App.FormsData.Views.Content;
using App.FormsData.Views.Main;
using App.Framework;
using Lottie.Forms;
using Xamarin.Forms;

namespace App.FormsData.Behaviors
{
    public class ClickAnimationButton : Behavior<View>
    {
        public ClickAnimationButton(AnimationView button, EventHandler acton)
        {
            this.button = button;
            this.acton = acton;
        }

        private AnimationView button;
        private EventHandler acton;


        protected override void OnAttachedTo(View view)
        {
            button.OnClick += acton;

            base.OnAttachedTo(view);
        }

        protected override void OnDetachingFrom(View entry)
        {
            button.OnClick -= acton;

            button = null;
            acton = null;

            base.OnDetachingFrom(entry);
        }
    }
}
