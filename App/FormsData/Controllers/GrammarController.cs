﻿using System;
using System.Collections.Generic;
using System.Text;
using App.Data.Modules.Directory;
using App.FormsData.Views.Content.Grammar;

namespace App.FormsData.Controllers.Content
{
    public class GrammarController : BaseController<GrammarController>
    {
        #region Init

        public Structure structure;

        public override void OnInit()
        {
         //   structure = GrammarUtility  .GetStructure();
        }

        public override void OnRelease()
        {
           
        }

        
        public GrammarContent GrammarContent { get; set; }



        public StructureElement[] GetAllElementInFolder(string name=null)
        {
            return null;


            //if (string.IsNullOrEmpty(name))
            //  return  structure.root;

            //var names = name.Split('.');

            //return Find(structure.root,names,0);
        }

        private StructureElement[] Find(StructureElement[] elements, string[] names, int iter)
        {
            if (iter == names.Length)
                return null;


            var name = names[iter];

            foreach (var e in elements)
            {
                if (e.IsFolder() && e.folder == name)
                {
                    if (iter == names.Length - 1)
                    {
                        return e.elements;
                    }
                    else
                    {
                        iter++;
                        return Find(e.elements, names, iter);
                    }
                }
            }
            return null;
        }




        #endregion
    }
}
