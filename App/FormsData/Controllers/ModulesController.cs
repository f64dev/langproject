﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using App.Data.Modules;
using App.FormsData.Behaviors;
using App.FormsData.Services;
using App.FormsData.Tools;
using App.FormsData.Views;
using App.FormsData.Views.Content;
using App.FormsData.Views.Content.Grammar;
using App.FormsData.Views.Content.Training;
using App.FormsData.Views.Main;
using App.Framework;
using Xamarin.Forms;

namespace App.FormsData.Controllers
{
    public class ModulesController : BaseController<ModulesController>
    {
        #region Controller

        public override void OnInit()
        {
            AppFramework.SetLayout(PageLayoutType.BarAndContent);

            var bar = AppFramework.Bar;
            var content = AppFramework.Content;


            bar.BackgroundColor = Color.Azure;

            content.Add(CreateCurrentContentContainer());
        }

        public override void OnRelease()
        {

        }

        #endregion
        #region Animations

        private Animation testAnimation;


        private void InitAnimations()
        {
            testAnimation = new Animation();

            testAnimation.Add(0, 1, Animations.TopToBottom());
            testAnimation.Add(0, 0.5, Animations.BarTitleFade(finished: () =>
            {
              //  PageController.Get.Bar.SetLayout(BarLayout.Content | BarLayout.RightButton);
              //  PageController.Get.Bar.Content.Title.Text = "Test";
            }));
            testAnimation.Add(0.5, 1, Animations.BarTitleUnFade());
        }

        #endregion
        #region Menu

        public MenuContent CreateMenu() => new MenuContent();

        #endregion
        #region Modules

        public View CreateCurrentContentContainer()
        {
            View container = null;

            switch (AppSetting.CurrentModuleType)
            {
                case ModuleType.Training:
                    container= new TrainingContainer();
                    break;
                case ModuleType.Grammar:
                    container = new GrammarContent();
                    break;
            }

            return container;
        }

        #endregion
    }
}
