﻿using System;
using System.Collections.Generic;
using System.Text;
using App.Data.Modules.Learning;
using App.FormsData.Services;
using App.FormsData.Views.Content.Training.Timeline;
using App.FormsData.Views.Training.Timeline.Tracks;
using Xamarin.Forms;

namespace App.FormsData.Controllers
{
    public class TrainingController : BaseController<TrainingController>
    {
        #region Controller

        public override void OnInit()
        {
          
        }

        public override void OnRelease()
        {
          
        }

        #endregion


        public TimelineStack TimelineStack { get; set; }


        public TimelineBackground TimelineBackground { get; set; }

        public string CurrentTimelineType
        {
            get => AppSetting.GetSetting<string>(nameof(CurrentTimelineType));
            set => AppSetting.SetSetting(nameof(CurrentTimelineType), value);
        }

        public double CurrentTimelinePosition
        {
            get => AppSetting.GetSetting<double>(nameof(CurrentTimelinePosition));
            set => AppSetting.SetSetting(nameof(CurrentTimelinePosition), value);
        }



        public TimelineTrackType CurrentTimelineTrackType
        {
            get => (TimelineTrackType)AppSetting.GetSetting<int>(nameof(CurrentTimelineTrackType));
            set => AppSetting.SetSetting(nameof(CurrentTimelineTrackType), (int)value);
        }



        public static View GetTrackView(TimelineTrack track, int count)
        {
            switch (track.type)
            {
                case TimelineTrackType.Theory:
                    return new TheoryTrack((TimelineTrack)track, (int)count);
                case TimelineTrackType.Practice:
                    return new PracticeTrack((TimelineTrack)track, (int)count);
            }

          

            return null;
        }




    }
}
