﻿using System;
using System.Collections.Generic;
using System.Text;
using App.Data.Playables;
using App.Data.Playables.Theory;
using App.Data.Playables.Track;
using App.FormsData.Views.Playable;
using App.Framework;
using Xamarin.Forms;

namespace App.FormsData.Controllers
{
    public class PlayablePlayerController : BaseController<PlayablePlayerController>
    {
        #region Animation

        private Animation startAnimation;

        private Animation endAnimation;

        private void InitAnimation()
        {
            startAnimation = new Animation();
            endAnimation = new Animation();
        }

        #endregion
        #region End

        public Action<PlayableTrack> end;

        #endregion
        #region Init

        public override void OnInit()
        {
            InitAnimation();
        }

        public override void OnRelease()
        {
            
        }

        public void Start(PlayableTrack track, int position = 0)
        {
            Track = track;
            Position = position;

            isStart = true;

         //   PageController.Page.Content.PushAnimation = null;

          //  PageController.Page.Content.Push(GetPlayableView(CurrentPlayable), 500, isClear: true);
        }




        #endregion
        #region Track

        private bool isStart;

        public PlayableTrack Track { get; private set; }
        public int Position { get; private set; }


        public void Next()
        {
         

            Position++;

            if (Position >= Track.elements.Count)
            {
                end?.Invoke(Track);

                return;
            }


          //  PageController.Page.Content.Push(GetPlayableView(CurrentPlayable), 500, isClear: true);
        }

        public View CurrentView { get => isStart ? AppFramework.Content.CurrentElement : null; }
        public BasePlayable CurrentPlayable { get => isStart ? Track.elements[Position] : null; }

        public View GetPlayableView(BasePlayable playable)
        {
            switch (playable)
            {
                case SimpleTheoryPlayable p:
                    return new SimpleTheoryPlayableView(p);

                case ListTheoryPlayable p:
                    return new ListTheoryPlayableView(p);
            }

            return null;
        }

        #endregion
    }
}
