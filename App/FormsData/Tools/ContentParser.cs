﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace App.FormsData.Tools
{
    public static class ContentParser
    {

        public static bool TryGetAudio(string str, out string audio)
        {
            if (str.StartsWith("audio"))
            {
                audio = str.Substring(6, str.Length - 7);

                return true;
            }
            audio = null;
            return false;
        }

        public static bool IsBold(string str) => HasString(str,"bold");
 
        public static bool IsTrans(string str, out string param)=> HasString(str,"trans", out param);
        public static bool IsAudio(string str, out string param) => HasString(str, "audio", out param);


        private static bool HasString(string str,string value)=> str.StartsWith(value);

       private static bool HasString(string str, string value, out string param)
        {
            if (str.StartsWith(value))
            {
                param = str.Substring(value.Length+1, str.Length - (value.Length + 2));

                return true;
            }
            param = null;
            return false;
        }




    }
}
