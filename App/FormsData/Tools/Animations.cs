﻿using System;
using System.Collections.Generic;
using System.Text;
using App.FormsData.Controllers;
using App.Framework;
using Xamarin.Forms;

namespace App.FormsData.Tools
{
    public static class Animations
    {
        public static Animation TopToBottom(Easing easing=null,Action finished = null) => new Animation((t) =>
        {
            var content = AppFramework.Content;
            content.CurrentElement.TranslationY = content.Height * (t - 1);
        },easing:easing,finished:finished);

        public static Animation BottomToTop(Easing easing = null, Action finished = null) => new Animation((t) =>
        {
            var content = AppFramework.Content;
            content.CurrentElement.TranslationY = content.Height * -t;
        }, easing: easing, finished: finished);

        public static Animation BarTitleFade(Easing easing = null, Action finished = null) => new Animation((t) =>
        {
            AppFramework.Bar.Content.Opacity = 1-t;
        }, easing: easing, finished: finished);

        public static Animation BarTitleUnFade(Easing easing = null, Action finished = null) => new Animation((t) =>
        {
            AppFramework.Bar.Content.Opacity = t;
        }, easing: easing, finished: finished);





    }
}
