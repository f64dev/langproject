﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace App.FormsData.Tools
{
    public static class XamarinTools
    {
        #region Font

        public static void SetDynamicFont(Button view, float factor)
        {
            view.SizeChanged += (object sender, EventArgs e) => { view.FontSize = (view.Height + view.Margin.Top + view.Margin.Bottom) * factor; };
        }


        public static void SetDynamicFont(Label view, float factor)
        {
            view.SizeChanged += (object sender, EventArgs e) => { view.FontSize = (view.Height + view.Margin.Top + view.Margin.Bottom) * factor; };
        }

        #endregion
        #region Animations


        public static void ChangeAndFadeText(Label label, uint len, String newText)
        {
            label.FadeTo(-1f, len / 2, Easing.CubicInOut).GetAwaiter().OnCompleted(() =>
            {
                label.Text = newText;
                label.FadeTo(1f, len / 2, Easing.CubicInOut);
            });
        }

        #endregion
    }
}
