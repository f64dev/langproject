﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.FormsData.Controllers
{
    public abstract class BaseController<T>:BaseController where T : BaseController
    {
        public static void Init()
        {
            controller = Activator.CreateInstance<T>();
            controller?.OnInit();
        }
        public static void Release()
        {
            controller?.OnRelease();
            controller=null;
        }


        private static T controller;
        public static T Get{ get => controller; }
    }
}
