﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.FormsData.Controllers
{
    public abstract class BaseController
    {
        public abstract void OnInit();

        public abstract void OnRelease();
    }
}
