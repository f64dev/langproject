﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Server.Shared.Requests;
using Server.Shared.Responses;

namespace App.REST
{
    public static class RESTService
    {
        #region Init

        private static HttpClient client;

        private const string apiUrl = "http://f128.ga/api/";


        public static void Init()
        {
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
        }

        #endregion
        #region Auth

        private const string registrationUrl = apiUrl + "auth/registration";

        public static async Task<RegistrationResponse> Registration(RegistrationRequest registrationData)
        {
            var uri = new Uri(registrationUrl);

            var json = JsonConvert.SerializeObject(registrationData);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await client.PostAsync(uri, content);

            if (!response.IsSuccessStatusCode)
                return null;

            return JsonConvert.DeserializeObject<RegistrationResponse>(await response.Content.ReadAsStringAsync());
        }


        private const string testUrl = apiUrl + "auth/test";

        public static async Task<RegistrationResponse> Test(RegistrationRequest registrationData)
        {
            var uri = new Uri(registrationUrl);

            var json = JsonConvert.SerializeObject(registrationData);
            var content = new StringContent(json, Encoding.UTF8, "application/json");


            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1MzA4MDE5NDgsImlzcyI6IkVuZ2xpc2hBcHBTZXJ2ZXIiLCJhdWQiOiJFbmdsaXNoQXBwIn0.vodqK2Wp31acAn9-2feJ5GDOZBaoaXJwZplUdALCnAY");

            var response = await client.GetAsync(uri);

            if (!response.IsSuccessStatusCode)
                return null;

            return JsonConvert.DeserializeObject<RegistrationResponse>(await response.Content.ReadAsStringAsync());
        }

        #endregion

    }
}
