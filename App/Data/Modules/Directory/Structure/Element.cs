﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Data.Modules.Directory
{
   [Serializable]
   public class StructureElement
    {
        public string name;
        public string folder;
        public string file;

        public StructureElement[] elements;



        [NonSerialized]
        public string path;

        public bool IsFolder() => folder != null;
    }
}
