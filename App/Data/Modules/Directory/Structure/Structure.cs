﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace App.Data.Modules.Directory
{
    [Serializable]
    public class Structure
    {
        public StructureElement[] root;

        public void Init()
        {
            SetPath(root,null);
        }

        private void SetPath(StructureElement[] elements,string path)
        {
            foreach (var e in elements)
            {
                var p = e.IsFolder() ? e.folder : e.file;

                if (path == null)
                    e.path = p;
                else
                    e.path = $"{path}.{p}";

                if (e.IsFolder())
                    SetPath(e.elements, e.path);
            }
        }
    }
}
