﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace App.Data.Modules.Directory
{
   public class UnitElement
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public ElementType type;

        public string content;
    }
}
