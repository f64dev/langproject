﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Data.Modules.Directory
{
    [Serializable]
    public class Unit
    {
        public string name;

        public UnitElement[] elements;
    }
}
