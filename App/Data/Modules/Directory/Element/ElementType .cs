﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Data.Modules.Directory
{
    public enum ElementType
    {
        Label,
        Text,
        Image,
        EndabbedImage,
        TextList,
        TextTable
    }
}
