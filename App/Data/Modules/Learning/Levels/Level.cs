﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Data.Modules.Learning
{
    [Serializable]
    public class Level
    {
        public string name;
        public string folder;
    }
}
