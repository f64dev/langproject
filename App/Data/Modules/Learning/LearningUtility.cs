﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Newtonsoft.Json;

namespace App.Data.Modules.Learning
{
    public static class LearningUtility
    {
        public const string nameLocalFolderRoot = "Learning";
        public const string nameLevelsFile = "Levels";
        public const string nameLevelFile = "Level";
    }
}
