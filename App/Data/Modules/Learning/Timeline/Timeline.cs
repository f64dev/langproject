﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Data.Modules.Learning
{
    [Serializable]
    public sealed class Timeline
    {
        public string name;
        public string folder;

        public TimelineTrack[] root;

        public void Init()
        {
            foreach (var item in root)
            {
                foreach (var e in item.elements)
                    e.path = $"{folder}.{ item.folder}.{e.file}";
            }
        }

        public TimelineTrack GetTrack(TimelineTrackType type)
        {
            foreach (var e in root)
                if (e.type == type)
                    return e;

            return null;
        }

        public int GetTimelineLength()
        {
            var count = 0;

            foreach (var t in root)
                foreach (var e in t.elements)
                    if (e.pos > count)
                        count = e.pos;

            return count;
        }
    }
}
