﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace App.Data.Modules.Learning
{
    [Serializable]
    public class TimelineTrack
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public TimelineTrackType type;

      
        public string folder;


        public TimelineElement[] elements;


        public TimelineElement GetElement(int pos)
        {
            foreach (var e in elements)
                if (e.pos == pos)
                    return e;

            return null;
        }

    }
}
