﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Data.Modules.Learning
{
    public enum TimelineTrackType
    {
        Theory,
        Practice
    }
}
