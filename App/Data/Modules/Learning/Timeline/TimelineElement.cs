﻿using System;

namespace App.Data.Modules.Learning
{
    [Serializable]
    public class TimelineElement
    {
        public int pos;

        public string name;
        public string file;

        [NonSerialized]
        public string path;
    }
}
