﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Data.Playables.Theory
{
    public class SimpleTheoryPlayable : BasePlayable
    {
        public string Title { get; set; }
        public string Text { get; set; }
    }
}
