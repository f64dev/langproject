﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Data.Playables.Theory
{
    public class ListTheoryPlayable : BasePlayable
    {
        public string Title { get; set; }
        public string Text { get; set; }

        public string[] List { get; set; }
    }
}
