﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Data.Playables.Theory
{
    public class TableTheoryPlayable : BasePlayable
    {
        public string Title { get; set; }
        public string Label { get; set; }
        public string Table { get; set; }
    }
}
