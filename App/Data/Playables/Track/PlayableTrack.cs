﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Data.Playables.Track
{
  public  class PlayableTrack
    {
        public string name;

        public List<BasePlayable> elements;
    }
}
